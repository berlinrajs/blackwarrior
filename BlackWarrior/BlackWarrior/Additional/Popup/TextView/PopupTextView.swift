//
//  AlertView.swift
//  ProDental
//
//  Created by Berlin Raj on 20/09/16.
//  Copyright © 2016 SRS. All rights reserved.
//

import UIKit


class PopupTextView: UIView {
    
    class func popUpView() -> PopupTextView {
        return Bundle.main.loadNibNamed("PopupTextView", owner: nil, options: nil)!.first as! PopupTextView
    }
    
    @IBOutlet weak var textView: MCTextView!
    
    var completion:((_ popUpView: PopupTextView, _ textView: MCTextView)->Void)?
    
    func show(_ completion : @escaping (_ popUpView: PopupTextView, _ textView : MCTextView) -> Void) {
        self.showWithPlaceHolder("IF YES, TYPE HERE", completion: completion)
    }
    
    func showWithPlaceHolder(_ placeHolder : String, completion : @escaping (_ popUpView: PopupTextView, _ textView : MCTextView) -> Void) {
        textView.placeholder = placeHolder
        self.completion = completion
        
        self.frame = CGRect(x: 0, y: 0, width: screenSize.width, height: screenSize.height)
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        appDelegate.window!.addSubview(self)
        self.subviews[0].transform = CGAffineTransform(scaleX: 0.1, y: 0.1)
        textView.placeholder = placeHolder
        textView.placeholderColor = UIColor.lightGray
        UIView.beginAnimations(nil, context: nil)
        UIView.setAnimationDuration(0.3)
        self.subviews[0].transform = CGAffineTransform.identity
        UIView.commitAnimations()
    }
    
    @IBAction func buttonActionOK(_ sender: AnyObject) {
        completion?(self, self.textView)
    }
    
    func close() {
        self.removeFromSuperview()
    }
}
