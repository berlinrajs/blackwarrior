//
//  ActionImageView.swift
//  AceDental
//
//  Created by Berlin Raj on 20/09/16.
//  Copyright © 2016 SRS. All rights reserved.
//

import UIKit

class ActionImageView: UIImageView {

    fileprivate var labelTitle: UILabel!
    override func draw(_ rect: CGRect, for formatter: UIViewPrintFormatter) {
        
    }
    override func draw(_ rect: CGRect) {
        super.draw(rect)
        layer.borderColor = borderColor.cgColor
        layer.borderWidth = borderWidth
        layer.cornerRadius = cornerRadius
        clipsToBounds = true
    }
    override func willMove(toSuperview newSuperview: UIView?) {
        super.willMove(toSuperview: newSuperview)
        layer.borderColor = borderColor.cgColor
        layer.borderWidth = borderWidth
        layer.cornerRadius = cornerRadius
        clipsToBounds = true
    }
    
    var borderColor: UIColor = UIColor.white {
        didSet {
            layer.borderColor = borderColor.cgColor
        }
    }
    
    var cornerRadius: CGFloat = 3.0 {
        didSet {
            layer.cornerRadius = cornerRadius
            clipsToBounds = true
        }
    }
    
    var borderWidth: CGFloat = 1.0 {
        didSet {
            layer.borderWidth = borderWidth
        }
    }
    var title: String = "" {
        willSet {
            if labelTitle == nil {
                labelTitle = UILabel(frame: CGRect(x: 0, y: 0, width: self.frame.width, height: self.frame.height))
                labelTitle.font = UIFont.systemFont(ofSize: 15)
                labelTitle.textColor = UIColor.white
                labelTitle.textAlignment = NSTextAlignment.center
                
                self.addSubview(labelTitle)
            }
        }
        didSet {
            self.labelTitle.text = title
        }
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        if labelTitle != nil {
            labelTitle.frame = CGRect(x: 0, y: 0, width: self.frame.width, height: self.frame.width)
        }
    }
    
    func addTarget(_ target: AnyObject, selector: Selector) {
        self.isUserInteractionEnabled = true
        
        let tapGesture = UITapGestureRecognizer(target: target, action: selector)
        tapGesture.numberOfTapsRequired = 1
        self.addGestureRecognizer(tapGesture)
    }

}
