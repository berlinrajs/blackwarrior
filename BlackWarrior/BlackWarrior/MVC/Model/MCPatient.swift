//
//  MCPatient.swift
//  WestgateSmiles
//
//  Created by Leojin Bose on 2/19/16.
//  Copyright © 2016 SRS. All rights reserved.
//

import UIKit


class MCPatient: NSObject {
    
    required override init() {
        super.init()
    }
    
    init(forms : [Forms]) {
        super.init()
        self.selectedForms = forms
    }
    
    #if AUTO
    var patientDetails : PatientDetails?
    #endif
    
    var gender: Gender!
    var maritalStatus: MaritalStatus!
  
    
    var selectedForms : [Forms]!
    var firstName : String!
    var lastName : String!
    var initial : String!
    var prefferedName : String!

    var dateOfBirth : String!
    var dateToday : String!
    var dentistName: String!
    
    var parentFirstName: String!
    var parentLastName: String!
    var parentDateOfBirth: String!
    
    var is18YearsOld : Bool {
        return patientAge >= 18
    }
    
    var fullName: String {
        return initial.characters.count > 0 ? firstName + " " + initial + " " + lastName : firstName + " " + lastName
    }
    var patientAge: Int! {
        get {
            if dateOfBirth == nil || dateOfBirth.characters.count == 0 {
                return 0
            }
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = kCommonDateFormat
            
            let numberOfDays = (Date().timeIntervalSince(dateFormatter.date(from: self.dateOfBirth)!))/(3600 * 24)
            return Int(numberOfDays/365.2425)
        }
    }
    
    var address: String!
    var city: String!
    var state: String!
    var zip: String!
    var phone: String!
    var socialSecurityNumber: String!
    var workPhone: String!
    var cellPhone: String!
    var email: String!
    
//    var maritalStatus: Int!
//    var gender: Int!
    var isEmployed: Bool!
    var isResponsibleParty: Bool!
    var havePrimaryInsurance: Bool!
    var haveSecondaryInsurance: Bool!
    
    var employerName: String!
    var employerPhone: String!
    
    var emergencyName: String!
    var emergencyPhone: String!
    
    var responsibleRelation: Int!
    var responsibleName: String!
    var responsibleAddress: String!
    var responsibleCity: String!
    var responsibleState: String!
    var responsibleZip: String!
    var responsibleGroup: String!
    var responsibleOtherRelation: String!
    var signatureResponsible: UIImage!
    
    var primaryInsurance: Insurance!
    var secondaryInsurance: Insurance!
    
    var visitorFirstName : String!
    var visitorLastName : String!
    
    var visitorFullName : String {
        return visitorFirstName + " " + visitorLastName
    }
    var visitingPurpose : String!
    
    var signatureFinancialPolicy: UIImage!
    var signatureAppointmentPolicy: UIImage!
    var signatureReminderPolicy: UIImage!
    
    var reminderPhoneNumber: String!
    var reminderEmail: String!
    var isAllowText: Bool!
    var isAllowEmail: Bool!
    
    //New patient
    var responsibleParty : MCPatient!
    
    var formID: String!
    var patientPolicyButton : Int!
    var driverLicense: String!
    var workPhoneExt: String!
    var employerID : String?
    var medicaidID : String?
    var carrierID : String?
    var prefDentist : String?
    var prefPharm : String?
    var preHygen : String?
    var previousDoctor : String?
    var referredBy : String?
    var emergencyContactName : String!
    var emergencyContactPhoneNumber : String!
    var receieveEmail : Bool!
    var employeeStatus : Int?
    var studentStaus : Int?
    
    var Sec3CellPhone : String!
    var Sec3OtherAddress : String!
    var Sec3FreqProphy : String!
    var Sec3FreqPerioMaint : String!
    var Sec3FreqSrp : String!
    var Sec3cardType : String!
    var Sec3cardNumber : String!
    var Sec3expiryDate : String!
    var responsiblepartyType : Int!
    
    var signature1: UIImage!
    var signature2: UIImage!
    var signature3: UIImage!

    //Medical History
    var medicalHistoryQuestions1 : [MCQuestion]!
    var medicalHistoryQuestions2 : [MCQuestion]!
    var medicalHistoryQuestions3 : [MCQuestion]!
    var medicalHistoryQuestions4 : [MCQuestion]!
    var isWomen : Bool!
    var controlledSubstances : String!
    var controlledSubstancesClicked : Bool!
    var othersTextForm3 : String!
    var comments : String!
    var otherIllness : String!
    var buttonIllness: Int!
    var medicalSignature: UIImage!
    
    var paymentInitials: NSMutableDictionary!
    var paymentSignature: UIImage!
    
    var privacySignature: UIImage!
    var privacyRelation: String!
    var privacyName: String!
    
    //FINANCIAL AGREEMENT
    var responsiblePartyName : String = ""
    
    var witnessName : String = ""
    var liabilityDentistName : String = ""
    
    var dentalProcedures : String = ""
    var apointedTime : String = ""
    var duration : String = ""
    
    
    ///PATIENT SATISFACTION SURVEY
    var satisfaction : PatientSatisfaction = PatientSatisfaction()
    
    var referralPhoneNumber : String = ""
    var referralParentName : String = ""
    var referralParentPhoneNumber : String = ""
    var referralDentist : String = ""
    var referralRE : String = ""
    
    var relevantHistory : String = ""
    var appointmentDate : String = ""
    var buttonTags : [Int] = [Int]()
    
    
    //authorization
    var authorizeSS : String!
    var authorizeInitDate: String!
    var authorizeClientName: String!
    var authorizeClientRelation: String!
    var authorizeClientOrProviderOptionTag : Int! = 0
    
    var authorizeMultiArray1 : NSMutableArray!
    var authorizeRadio1: Int! = 0
    var authorizeRadio2: Int! = 0
    
    var myDentalInfo: String!
    var mostRecYears: String!
    var dentalRecDates: String!
    var otherInfo: String!
    
    var authorizeMultiArray2 : NSMutableArray!
    var authorizFamilyMembers : String!
    var authorizeOtherInfo2 : String!
    
    var radioAuthorize: Int! = 0
    var makeDisclosure: String!
    var receiveDisclosure: String!
    var expireDate : String!
    var followingEvent : String!
    
    var relationshipToClientIfPersonalRep: String!
    var authorizeAddress: String!
    
    //Medical Release for dental treatment
    var futureDate: String!
    var patientDate: String!
    var followingDentalTreatment: String!
    
    var beforeProceding: String!
    var orTakingFollowing: String!
    var inYourOpinion: String!
    
    var medicationRecomend: String!
    var otherRecomend: String!
    var physicianPhone: String!
    var physicianFax: String!
    
    var radioPreMed: Int! = 2
    var preMedication: String!
    
    var specialistPhone: String!
    var specialistFax: String!

    var oralProcedure : String = ""
    var oralComments : String = ""
    var oralRepresentativeName : String = ""
    var oralRepresentativeRelationship : String = ""
    var oralPatientSign : UIImage!
    var oralDoctorSign : UIImage!
    var radioSignTag : Int = 1
}

class Insurance: NSObject {
    var firstName: String!
    var lastName: String!
    var middleName: String!
    var isPatient: Bool!
    var birthDate: String?
    var id: String?
    var group: String?
    var street: String!
    var city: String!
    var state: String!
    var zip: String!
    
    var employerName: String!
    var employerStreet: String?
    var employerCity: String?
    var employerState: String?
    var employerZip: String?
    
    var selectedRelation: Int!
    var relationShip: String?
    //    NEW
    var name : String?
    var typeInsured : Int?
    var insuredSSN : String?
    var addressLine1 : String?
    var city1 : String?
    var state1 : String?
    var zipCode1 : String?
    var remBeneFits : String?
    var remDeduct : String?
    var insuranceCompany : String?
    var addressLine2 : String?
    var city2 : String?
    var state2 : String?
    var zipCode2 : String?
    
    var InsurancePlanNameAndAddress: String?
}

class PatientSatisfaction : NSObject{
    var ageTag : Int = 0
    var genderTag : Int = 0
    var visitTag : Int = 0
    var treatmentTag1 : Int = 0
    var treatmentTag2 : Int = 0
    
    var surveyQuestions : [[MCQuestion]]!
    var comments1 : String = ""
    var comments2 : String = ""
    var comments3 : String = ""
    var comments4 : String = ""

    var bestOffice : String = ""
    var leastOffice : String = ""
    var experience : String = ""
    
    var signPatient : UIImage!
    
    required override init() {
        super.init()
        let quest1: [String] = ["It was easy to make my first appointment.",
                                "The appointment secretary (coordinator) was polite and helpful.",
                                "I received a reminder of each of my appointments.",
                                "It was easy to schedule a convenient appointment.",
                                "Appointment options were given that suited my schedule.",
                                "I was seen on time for my appointments; if not, I was given a reason for the delay."]
        
        let quest2: [String] = ["The office location and parking were convenient.",
                                "The reception area was neat and clean.",
                                "The equipment was clean and presentable.",
                                "The temperature in the office was comfortable.",
                                "The lighting in the office was sufficient.",
                                "The music in the office was pleasant."]
        
        let quest3: [String] = ["The dentist was professional and courteous.",
                                "The dental hygienist was professional and courteous.",
                                "The dental assistant was professional and courteous.",
                                "The dentist was considerate and sensitive to my needs.",
                                "The dental hygientist was considerate and sensitive to my needs.",
                                "The dental assistant was considerate and sensitive to my needs.",
                                "Other office personnel were courteous and helpful."]
        
        let quest4: [String] = ["My proposed dental treatment was clearly explained.",
                                "Any questions I had were answered.",
                                "I was given treatment alternatives",
                                "My dental treatment was completed efficiently and in a timely manner.",
                                "I was pleased with the quality of my dental treatment.",
                                "The dental treatment was completed to my satisfaction.",
                                "The fees were explained prior to my treatment appointment.",
                                "The fees for service were fair.",
                                "I plan to remain a patient at this office."]
        
        
        self.surveyQuestions = [MCQuestion.getArrayOfQuestions(quest1),MCQuestion.getArrayOfQuestions(quest2),MCQuestion.getArrayOfQuestions(quest3),MCQuestion.getArrayOfQuestions(quest4)]
    }

}

enum MaritalStatus: String {
    case SINGLE = "S"
    case MARRIED = "M"
    case WIDOWED = "W"
    case DIVORCED = "D"
    case SEPARATE = "X"
    case UNKNOWN = "U"
    case DEFAULT = "DE"
    
    init(value: Int) {
        switch value {
        case 1: self = .SINGLE
        case 2: self = .MARRIED
        case 3: self = .WIDOWED
        case 4: self = .DIVORCED
        case 5: self = .SEPARATE
        case 6: self = .UNKNOWN
        default: self = .DEFAULT
        }
    }
    var index: Int {
        switch self {
        case .SINGLE: return 1
        case .MARRIED: return 2
        case .WIDOWED: return 3
        case .DIVORCED: return 4
        case .SEPARATE: return 5
        case .UNKNOWN: return 6
        default: return 7
        }
    }
    var description: String {
        switch self {
        case .SINGLE: return "SINGLE"
        case .MARRIED: return "MARRIED"
        case .WIDOWED: return "WIDOWED"
        case .DIVORCED: return "DIVORCED"
        case .SEPARATE: return "SEPARATED"
        case .UNKNOWN: return "UNKNOWN"
        default: return "DEFAULT"
        }
    }
}

enum Gender: String {
    case MALE = "M"
    case FEMALE = "F"
    case DEFAULT = "D"
    
    init(value: Int) {
        switch value {
        case 1: self = .MALE
        case 2: self = .FEMALE
        default: self = .DEFAULT
        }
    }
    var index: Int {
        switch self {
        case .MALE: return 1
        case .FEMALE: return 2
        default: return 3
        }
    }
    
    var description: String {
        switch self {
        case .MALE: return "MALE"
        case .FEMALE: return "FEMALE"
        default: return "DEFAULT"
        }
    }
}

#if AUTO
    class PatientDetails : NSObject {
        var dateOfBirth : String!
        var firstName : String!
        var lastName : String!
        var preferredName : String!
        var address : String!
        var city : String!
        var state : String!
        var zipCode : String!
        var country : String!
        var gender : Int!
        var socialSecurityNumber : String!
        var email : String!
        var homePhone : String!
        var workPhone : String!
        var cellPhone : String!
        
        
        var patientNumber : String!
        var maritalStatus: MaritalStatus?
        var middleInitial : String!
        
        override init() {
            super.init()
        }
        
        init(details : [String: AnyObject]) {
            super.init()
            self.city = getValue(details["City"])
            self.dateOfBirth = getValue(details["Birthdate"])
            self.email = getValue(details["Email"])
            self.socialSecurityNumber = getValue(details["SSN"])
            self.address = getValue(details["Address"])
            self.zipCode = getValue(details["Zip"])
            
            //self.gender = getValue(details["sex"]) == "" ? nil : Gender(rawValue: getValue(details["sex"]))
            //self.gender = getValue(details["sex"]) == "" ? nil : Gender(value: Int(getValue(details["sex"]))!)
            
            if let gen = details["sex"] as! String! {
                
                self.gender = (gen == "M" || gen == "0") ? 1 : 2
            } else{
                self.gender = Int(details["sex"]! as! String)! + 1
            }
            
            //self.gender = details["sex"] == nil ? 0 : Int(details["sex"]! as! String)! + 1
            
            self.firstName = getValue(details["FName"])
            self.patientNumber = getValue(details["PatNum"])
            self.state = getValue(details["State"])
            self.homePhone = getValue(details["HmPhone"]).formattedPhoneNumber
            self.workPhone = getValue(details["work_phone"]).formattedPhoneNumber
            self.cellPhone = getValue(details["cellular_phone"]).formattedPhoneNumber
            self.country = getValue(details["Country"])
            self.lastName = getValue(details["LName"])
            self.preferredName = getValue(details["Preferred"])
            
            //self.maritalStatus = getValue(details["marital_status"]) == "" ? nil : MaritalStatus(rawValue : getValue(details["marital_status"]))
            // self.maritalStatus = getValue(details["marital_status"]) == "" ? nil : MaritalStatus(value: Int(getValue(details["marital_status"]))!)
            
            if let mar = Int(details["marital_status"] as! String){
                
                self.maritalStatus = MaritalStatus(value: mar)
            }else{
                self.maritalStatus = MaritalStatus(rawValue : getValue(details["marital_status"]))
            }
            
            // self.maritalStatus = Int(details["marital_status"]! as! String)!
            
            self.middleInitial = getValue(details["MiddleI"])
        }
        
        func getValue(_ value: AnyObject?) -> String {
            
            if value is String {
                return (value as! String).uppercased()
            }
            return ""
        }
        
        
    }
#endif


