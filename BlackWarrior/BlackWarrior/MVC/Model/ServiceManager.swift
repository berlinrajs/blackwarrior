//
//  ServiceManager.swift
//   Angell Family Dentistry      
//
//  Created by Bose on 02/02/16.
//  Copyright © 2016  Angell Family Dentistry. All rights reserved.
//

import UIKit

//let hostUrl = "https://integrations.srswebsolutions.com/demoes/eaglesoft/" //demo
let hostUrl = "https://integrations.srswebsolutions.com/blackwarrior/eaglesoft/"   // live
//let hostUrl = "https://eaglesoft.srswebsolutions.com/eaglesoft/"

class ServiceManager: NSObject {
    
    class func fetchDataFromService(_ baseUrlString: String, serviceName: String, parameters : [String : String]?, success:@escaping (_ result : AnyObject) -> Void, failure :@escaping (_ error : NSError) -> Void) {
        
        let manager = AFHTTPSessionManager(baseURL: URL(string: baseUrlString))
        
        manager.responseSerializer.acceptableContentTypes = ["text/html"]
        manager.post(serviceName, parameters: parameters, progress: { (progress) in
        }, success: { (task, result) in
            success(result as AnyObject)
        }) { (task, error) in
            failure(error as NSError)
        }
    }
    
    class func fetchDataFromServiceCheckIn(_ serviceName: String, parameters : [String : String]?, success:@escaping (_ result : AnyObject) -> Void, failure :@escaping (_ error : NSError) -> Void) {
        
        let manager = AFHTTPSessionManager(baseURL: URL(string: "https://alpha.mncell.com/mconsent/"))
        
        manager.responseSerializer.acceptableContentTypes = ["text/html"]
        manager.post(serviceName, parameters: parameters, progress: { (progress) in
        }, success: { (task, result) in
            success(result as AnyObject)
        }) { (task, error) in
            failure(error as NSError)
        }
    }
    class func loginWithUsername(_ userName: String, password: String, completion: @escaping (_ success: Bool, _ error: NSError?) -> Void) {
        ServiceManager.fetchDataFromService("https://mncell.com/mclogin/", serviceName: "apploginapi.php?", parameters: ["appkey": kAppKey, "username": userName, "password": password], success: { (result) in
            if (result["posts"] as AnyObject)["status"] as! String == "success" {
                completion(true, nil)
            } else {
                completion(false, NSError(domain: "", code: 0, userInfo: [NSLocalizedDescriptionKey: (result["posts"] as AnyObject)["message"] as! String]))
            }
        }) { (error) in
            completion(false, nil)
        }
    }
    class func ChekInFormStatus(_ patientName: String, patientPurpose: String, completion: @escaping (_ success: Bool, _ error: NSError?) -> Void) {
        //"mcdistinctivelaser"
        ServiceManager.fetchDataFromServiceCheckIn("savepatients_info.php?", parameters: ["patientkey": kAppKey, "patientname": patientName, "patientpurpose": patientPurpose], success: { (result) in
            if (result["posts"] as! String == "success") {
                completion(true, nil)
            } else {
                completion(false, NSError(domain: "", code: 0, userInfo: [NSLocalizedDescriptionKey: (result["posts"] as AnyObject)["message"] as! String]))
            }
        }) { (error) in
            completion(false, nil)
        }
    }
    
    class func postReview(_ name: String, comment: String, rating: CGFloat, phoneNumber: String, allowMessage: Bool, email: String, anonymous: Bool, completion: @escaping (_ success: Bool, _ error: NSError?) -> Void) {
        ServiceManager.fetchDataFromService("https://alpha.mncell.com/review/", serviceName: "app_review.php?", parameters: ["patient_appkey": kAppKey, "patient_name": name, "patient_review_comment": comment, "patient_rating": "\(rating)", "patient_phone_number": phoneNumber, "sms_allowed": allowMessage == true ? "1" : "0", "patient_email": email, "user_anonymous": anonymous == true ? "1" : "0"], success: { (result) in
            if result["posts"] != nil && (result["posts"] as AnyObject)["status"] as! String == "success" {
                completion(true, nil)
            } else {
                completion(false, NSError(domain: "", code: 0, userInfo: [NSLocalizedDescriptionKey: (result["posts"] as AnyObject)["message"] as! String]))
            }
        }) { (error) in
            completion(false, NSError(domain: "", code: 0, userInfo: [NSLocalizedDescriptionKey: "Something went wrong, Please Check your internet connection and try again"]))
        }
    }
    #if AUTO
    class func sendPatientDetails(patient: MCPatient, completion:@escaping (_ success : Bool, _ error: Error?) -> Void) {
    let dateFormatter = DateFormatter()
    dateFormatter.dateFormat = "MMMM dd, yyyy"
    let date = dateFormatter.date(from: patient.dateOfBirth)
    dateFormatter.dateFormat = "yyyy-MM-dd"
    var params: [String: String] = [String: String]()
    params["Birthdate"] = dateFormatter.string(from: date!)
    params["FName"] = patient.firstName
    params["LName"] = patient.lastName
    params["MiddleI"] = patient.initial
    
    //        if let adult = patient.medicalHistory {
    params["City"] = patient.city
    params["Email"] = patient.email
    params["Address"] = patient.address
    params["Zip"] = patient.zip
    params["HmPhone"] = patient.phone.phoneNumber
    
    if let wPhone = patient.workPhone{
    params["work_phone"] = wPhone.phoneNumber
    }
    params["cellular_phone"] = patient.cellPhone.phoneNumber
    params["sex"] = String(patient.gender!.rawValue)
    params["marital_status"] = String(describing: patient.maritalStatus!)
    params["State"] = patient.state
    params["Preferred"] = patient.prefferedName
    
    if let ssn = patient.socialSecurityNumber {
    params["SSN"] = ssn.socialSecurityNumber
    }
    //        }
    
    
    if let patientDetails = patient.patientDetails {
    params["PatNum"] = patientDetails.patientNumber.isEmpty ? "0" : patientDetails.patientNumber
    } else {
    params["PatNum"] = "0"
    }
    let manager = AFHTTPSessionManager(baseURL: URL(string: hostUrl))
    manager.responseSerializer.acceptableContentTypes = ["text/html"]
    manager.post("consent_form_add_newpatient_op.php", parameters: params, progress: { (progress) in
    
    }, success: { (task, result) in
    if let patientId = (result as AnyObject)["patient_id"] as? String {
    if let patientDetails = patient.patientDetails {
    patientDetails.patientNumber = patientId
    } else {
    patient.patientDetails = PatientDetails()
    patient.patientDetails?.patientNumber = patientId
    }
    }
    completion(true, nil)
    }) { (task, error) in
    completion(false, error)
    }
    }
    #endif
    
    class func uploadFile(fileName: String, pdfData: Data, completion: @escaping (_ success: Bool, _ errorMessage: String?)-> Void) {
        
        //        http://consentforms.mncell.com/consent_form_upload_to_server.php
        #if AUTO
            DispatchQueue.main.async(execute: {
                let manager = AFHTTPSessionManager(baseURL: URL(string: hostUrl))
                manager.responseSerializer.acceptableContentTypes = ["text/html"]
                manager.post("consent_form_upload_to_server.php", parameters: nil, constructingBodyWith: { (data) in
                    data.appendPart(withFileData: pdfData, name: "consent_file", fileName: fileName, mimeType: fileName.hasSuffix(".pdf") ? "application/pdf" : "image/jpeg")
                }, progress: { (progress) in
                    print("Progress: \(progress)")
                }, success: { (task, result) in
                    completion(true, nil)
                }, failure: { (task, error) in
                    completion(false, error.localizedDescription)
                })
            })
        #else
            let manager = AFHTTPSessionManager(baseURL: URL(string: "https://consentforms.mncell.com/"))
            manager.responseSerializer.acceptableContentTypes = ["text/html"]
            
            manager.post("consent_form_upload_to_server.php", parameters: ["appkey": kAppKey], constructingBodyWith: { (data) in
                data.appendPart(withFileData: pdfData, name: "consent_file", fileName: fileName, mimeType: fileName.hasSuffix(".pdf") ? "application/pdf" : "image/jpeg")
            }, progress: { (progress) in
                
            }, success: { (task, result) in
                if let postDict = (result as! [String: AnyObject])["posts"] as? [String: AnyObject] {
                    if (postDict["status"] as? String)?.lowercased() == "success" {
                        completion(true, nil)
                    } else {
                        completion(false, postDict["message"] as? String)
                    }
                } else {
                    completion(false, "SOMETHING WENT WRONG, PLEASE CHECK YOUR INTERNET CONNECTION AND TRY AGAIN LATER")
                }
            }) { (task, error) in
                completion(false, error.localizedDescription)
            }
        #endif
    }
}
