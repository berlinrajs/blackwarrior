//
//  SchoolExcuseFormViewController.swift
//  BlackWarrior
//
//  Created by Bala Murugan on 12/18/16.
//  Copyright © 2016 Bala Murugan. All rights reserved.
//

import UIKit

class SchoolExcuseFormViewController: MCViewController {

    var signPatient : UIImage!
    @IBOutlet weak var labelPatientName : UILabel!
    @IBOutlet weak var labelDate : UILabel!
    @IBOutlet weak var patientSign : UIImageView!
    @IBOutlet weak var labelProcedure : UILabel!
    @IBOutlet weak var labelAppointedTime : UILabel!
    @IBOutlet weak var labelDuration : UILabel!
    

    override func viewDidLoad() {
        super.viewDidLoad()
        labelPatientName.text = patient.fullName
        labelDate.text = patient.dateToday
        patientSign.image = signPatient
        labelAppointedTime.text = patient.apointedTime
        labelDuration.text = patient.duration
        let str : NSString = labelProcedure.text!.replacingOccurrences(of: "KPROCEDURES", with: patient.dentalProcedures) as NSString
        let range = str.range(of: patient.dentalProcedures)
        let attributedString = NSMutableAttributedString(string: str as String)
        attributedString.addAttribute(NSUnderlineStyleAttributeName, value: NSUnderlineStyle.styleSingle.rawValue, range: range)
        labelProcedure.attributedText = attributedString
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    


}
