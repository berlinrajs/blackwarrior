//
//  SchoolExcuse1ViewController.swift
//  BlackWarrior
//
//  Created by Bala Murugan on 12/18/16.
//  Copyright © 2016 Bala Murugan. All rights reserved.
//

import UIKit

class SchoolExcuse1ViewController: MCViewController {

    @IBOutlet weak var textfieldProcedure : MCTextField!
    @IBOutlet weak var textfieldTime : MCTextField!
    @IBOutlet weak var textfieldDuration : MCTextField!
    @IBOutlet weak var signaturePatient : SignatureView!
    @IBOutlet weak var labelDate : DateLabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        textfieldTime.textFormat = .time
        labelDate.todayDate = patient.dateToday
        loadValue()

    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func loadValue()  {
        textfieldTime.text = patient.apointedTime
        textfieldDuration.text = patient.duration
        textfieldProcedure.text = patient.dentalProcedures
    }
    
    func saveValue() {
        patient.dentalProcedures = textfieldProcedure.isEmpty ? "" : textfieldProcedure.text!
        patient.apointedTime = textfieldTime.isEmpty ? "" : textfieldTime.text!
        patient.duration = textfieldDuration.isEmpty ? "" : textfieldDuration.text!
    }

    @IBAction override func buttonBackAction() {
        saveValue()
        super.buttonBackAction()
    }
    
    @IBAction func onNextButtonPressed (withSender sender : UIButton){
        if textfieldProcedure.isEmpty || textfieldTime.isEmpty || textfieldDuration.isEmpty{
            self.showAlert("PLEASE ENTER ALL THE REQUIRED FIELDS")
        }else if !signaturePatient.isSigned(){
            self.showAlert("PLEASE SIGN THE FORM")
        }else if !labelDate.dateTapped {
            self.showAlert("PLEASE SELECT THE DATE")
        }else{
            saveValue()
            let school = consentStoryBoard.instantiateViewController(withIdentifier: "SchoolExcuseFormVC") as! SchoolExcuseFormViewController
            school.patient = self.patient
            school.signPatient = signaturePatient.signatureImage()
            self.navigationController?.pushViewController(school, animated: true)
            
        }
    }


}
