//
//  Appointment1ViewController.swift
//  BlackWarrior
//
//  Created by Bala Murugan on 12/16/16.
//  Copyright © 2016 Bala Murugan. All rights reserved.
//

import UIKit

class Appointment1ViewController: MCViewController {

    @IBOutlet weak var signaturePatient : SignatureView!
    @IBOutlet weak var labelDate : DateLabel!

    override func viewDidLoad() {
        super.viewDidLoad()

        labelDate.todayDate = patient.dateToday
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func onNextButtonPressed (withSender sender : UIButton){
        if !signaturePatient.isSigned(){
            self.showAlert("PLEASE SIGN THE FORM")
        }else if !labelDate.dateTapped{
            self.showAlert("PLEASE SELECT THE DATE")
        }else{
            let appointment = consentStoryBoard.instantiateViewController(withIdentifier: "AppointmentFormVC") as! AppointmentFormViewController
            appointment.patient = self.patient
            appointment.signPatient = signaturePatient.signatureImage()
            self.navigationController?.pushViewController(appointment, animated: true)
            
        }
    }
    


}

