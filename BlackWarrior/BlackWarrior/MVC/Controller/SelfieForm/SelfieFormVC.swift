//
//  SelfieFormVC.swift
//  Bellevue Dental
//
//  Created by Berlin Raj on 13/12/16.
//  Copyright © 2016 SRS Web Solutions. All rights reserved.
//

import UIKit

class SelfieFormVC: MCViewController {

    @IBOutlet weak var imageView: UIImageView!
    
    @IBOutlet weak var labelName: UILabel!
    @IBOutlet weak var labelDate: UILabel!
    
 
    var selfieImage: UIImage!
    
    var isDrivingLicense: Bool = false
    override func viewDidLoad() {
        super.viewDidLoad()
        
        imageView.layer.borderColor = UIColor.lightGray.cgColor
        imageView.layer.cornerRadius = 3.0
        imageView.layer.masksToBounds = true
        imageView.layer.borderWidth = 1.0
        
        labelDate.text = "DATE: " + patient.dateToday
        
        labelName.text = "PATIENT NAME: " + patient.fullName.uppercased()
        
        self.imageView.image = selfieImage
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
        

}
