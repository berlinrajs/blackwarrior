//
//  Referral1ViewController.swift
//  BlackWarrior
//
//  Created by Bala Murugan on 12/19/16.
//  Copyright © 2016 Bala Murugan. All rights reserved.
//

import UIKit

class Referral1ViewController: MCViewController {

    @IBOutlet weak var textfieldPhoneNumber : MCTextField!
    @IBOutlet weak var textfieldParentName : MCTextField!
    @IBOutlet weak var textfieldParentPhoneNumber : MCTextField!
    @IBOutlet weak var textfieldDentist : MCTextField!
    @IBOutlet weak var textviewRE : MCTextView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        loadValue()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func loadValue()  {
        textfieldPhoneNumber.textFormat = .phone
        textfieldParentPhoneNumber.textFormat = .phone
        textviewRE.placeholder = "PLEASE TYPE HERE"
        textviewRE.placeholderColor = UIColor.lightGray
        textfieldPhoneNumber.text = patient.referralPhoneNumber
        textfieldParentName.text = patient.referralParentName
        textfieldParentPhoneNumber.text = patient.referralParentPhoneNumber
        textfieldDentist.text = patient.referralDentist
        textviewRE.text = patient.referralRE == "" ? "PLEASE TYPE HERE" : patient.referralRE
        textviewRE.textColor = patient.referralRE == "" ? UIColor.lightGray : UIColor.black
        
    }
    
    func saveValue()  {
        patient.referralPhoneNumber = textfieldPhoneNumber.isEmpty ? "" : textfieldPhoneNumber.text!
        patient.referralParentName = textfieldParentName.isEmpty ? "" : textfieldParentName.text!
        patient.referralParentPhoneNumber = textfieldParentPhoneNumber.isEmpty ? "" : textfieldParentPhoneNumber.text!
        patient.referralDentist = textfieldDentist.isEmpty ? "" : textfieldDentist.text!
        patient.referralRE = textviewRE.isEmpty ? "" : textviewRE.text!
    }

    @IBAction override func buttonBackAction() {
        saveValue()
        super.buttonBackAction()
    }

    @IBAction func onNextButtonPressed (withSender sender : UIButton){
        if textfieldParentName.isEmpty || textfieldDentist.isEmpty{
            self.showAlert("PLEASE ENTER ALL THE REQUIRED FIELDS")
        }else if !textfieldPhoneNumber.isEmpty && !textfieldPhoneNumber.text!.isPhoneNumber{
            self.showAlert("PLEASE ENTER THE VALID PATIENT PHONE NUMBER")
        }else if !textfieldParentPhoneNumber.isEmpty && !textfieldParentPhoneNumber.text!.isPhoneNumber{
            self.showAlert("PLEASE ENTER THE VALID PARENT/GUARDIAN PHONE NUMBER")
        }else{
            saveValue()
            let referral = consentStoryBoard.instantiateViewController(withIdentifier: "Referral2VC") as! Referral2ViewController
            referral.patient = self.patient
            self.navigationController?.pushViewController(referral, animated: true)

        }
    }

}
