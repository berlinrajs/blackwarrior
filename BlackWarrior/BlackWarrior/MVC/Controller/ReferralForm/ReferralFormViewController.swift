//
//  ReferralFormViewController.swift
//  BlackWarrior
//
//  Created by Bala Murugan on 12/19/16.
//  Copyright © 2016 Bala Murugan. All rights reserved.
//

import UIKit


class ReferralFormViewController: MCViewController {
    
    var signPatient : UIImage!
    @IBOutlet weak var labelPatientName : UILabel!
    @IBOutlet weak var labelPatientPhone : UILabel!
    @IBOutlet weak var labelParentName : UILabel!
    @IBOutlet weak var labelParentPhone : UILabel!
    @IBOutlet weak var labelReferringDentist : UILabel!
    @IBOutlet      var labelRE : [UILabel]!
    @IBOutlet      var labelRelevantHistory : [UILabel]!

    @IBOutlet weak var buttonAppointment : UIButton!
    @IBOutlet weak var labelAppointment : UILabel!
    @IBOutlet      var arrayButtons : [UIButton]!
    @IBOutlet weak var signaturePatient : UIImageView!
    @IBOutlet weak var labelDate : UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        labelPatientName.text = patient.fullName
        labelPatientPhone.text = patient.referralPhoneNumber
        labelParentName.text = patient.referralParentName
        labelParentPhone.text = patient.referralParentPhoneNumber
        labelReferringDentist.text = patient.referralDentist
        patient.referralRE.setTextForArrayOfLabels(labelRE)
        patient.relevantHistory.setTextForArrayOfLabels(labelRelevantHistory)
        buttonAppointment.isSelected = patient.appointmentDate != ""
        labelAppointment.text = patient.appointmentDate
        for btn in arrayButtons{
            btn.isSelected = patient.buttonTags.contains(btn.tag)
        }
        signaturePatient.image = signPatient
        labelDate.text = patient.dateToday

    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    


}
