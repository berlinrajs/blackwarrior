//
//  Referral2ViewController.swift
//  BlackWarrior
//
//  Created by Bala Murugan on 12/19/16.
//  Copyright © 2016 Bala Murugan. All rights reserved.
//

import UIKit

class Referral2ViewController: MCViewController {

    @IBOutlet weak var textviewRelevantHistory : MCTextView!
    @IBOutlet weak var textfieldAppointmentDate : MCTextField!
    @IBOutlet      var arrayButtons : [UIButton]!
    @IBOutlet weak var signaturePatient : SignatureView!
    @IBOutlet weak var labelDate : DateLabel!
    @IBOutlet weak var labelPatientName : UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        labelDate.todayDate = patient.dateToday
        labelPatientName.text = patient.fullName
        loadValue()

    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    func loadValue()  {
        textfieldAppointmentDate.textFormat = .dateInCurrentYear
        textviewRelevantHistory.placeholderColor = UIColor.lightGray
        textviewRelevantHistory.placeholder = "PLEASE TYPE HERE"
        textviewRelevantHistory.text = patient.relevantHistory == "" ? "PLEASE TYPE HERE" : patient.relevantHistory
        textviewRelevantHistory.textColor = patient.relevantHistory == "" ? UIColor.lightGray : UIColor.black
        textfieldAppointmentDate.text = patient.appointmentDate
        for btn in arrayButtons{
            btn.isSelected = patient.buttonTags.contains(btn.tag)
        }
        
    }
    
    func saveValue()  {
        patient.relevantHistory = textviewRelevantHistory.isEmpty ? "" : textviewRelevantHistory.text!
        patient.appointmentDate = textfieldAppointmentDate.isEmpty ? "" : textfieldAppointmentDate.text!
        
    }
    
    @IBAction func buttonActionAppropriate (withSender sender : UIButton){
        sender.isSelected = !sender.isSelected
        if patient.buttonTags.contains(sender.tag){
            patient.buttonTags.remove(at: patient.buttonTags.index(of: sender.tag)!)
        }else{
            patient.buttonTags.append(sender.tag)
        }
    }
    @IBAction override func buttonBackAction() {
        saveValue()
        super.buttonBackAction()
    }
    
    @IBAction func onNextButtonPressed (withSender sender : UIButton){
        if !signaturePatient.isSigned(){
            self.showAlert("PLEASE SIGN THE FORM")
        }else if !labelDate.dateTapped{
            self.showAlert("PLEASE SELECT THE DATE")
        }else{
            saveValue()
            let referral = consentStoryBoard.instantiateViewController(withIdentifier: "ReferralFormVC") as! ReferralFormViewController
            referral.patient = self.patient
            referral.signPatient = signaturePatient.signatureImage()
            self.navigationController?.pushViewController(referral, animated: true)
            
        }
    }


}
