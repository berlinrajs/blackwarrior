//
//  PatientRegistrationStep2ViewController.swift
//  WestgateSmiles
//
//  Created by Leojin Bose on 3/2/16.
//  Copyright © 2016 SRS. All rights reserved.
//

import UIKit

class PatientRegistrationStep2ViewController: MCViewController {
    
    @IBOutlet weak var radioButton: RadioButton!
    @IBOutlet weak var textFieldAddressLine: MCTextField!
    @IBOutlet weak var textFieldState: MCTextField!
    @IBOutlet weak var textFieldCity: MCTextField!
    @IBOutlet weak var textFieldZipCode: MCTextField!
    @IBOutlet weak var textFieldPhone: MCTextField!
    @IBOutlet weak var textFieldSecurityNumber: MCTextField!
    @IBOutlet weak var textFieldWorkPhone: MCTextField!
    @IBOutlet weak var textFieldExt: MCTextField!
    @IBOutlet weak var textFieldBirthDate: MCTextField!
    @IBOutlet weak var textFieldDriverLicense: MCTextField!
    @IBOutlet weak var textFieldCellular: MCTextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        textFieldState.textFormat = .state
        textFieldBirthDate.textFormat = .dateIn1980
        textFieldZipCode.textFormat = .zipcode
        textFieldSecurityNumber.textFormat = .socialSecurity
        textFieldPhone.textFormat = .phone
        textFieldWorkPhone.textFormat = .phone
        textFieldCellular.textFormat = .phone
        textFieldExt.textFormat = .extensionCode
        
        loadValues()
    }
    
    func loadValues() {
        if patient.responsibleParty == nil {
            patient.responsibleParty = MCPatient()
        }
        radioButton.setSelectedWithTag(patient.responsibleParty.responsiblepartyType == nil ? 0 : patient.responsibleParty.responsiblepartyType)
        textFieldAddressLine.text = patient.responsibleParty.address
        textFieldCity.text = patient.responsibleParty.city
        textFieldState.text = patient.responsibleParty.state
        textFieldZipCode.text = patient.responsibleParty.zip
        textFieldPhone.text = patient.responsibleParty.phone
        textFieldBirthDate.text = patient.responsibleParty.dateOfBirth
        textFieldCellular.text = patient.responsibleParty.cellPhone
        
        textFieldExt.text = patient.responsibleParty.workPhoneExt
        textFieldSecurityNumber.text = patient.responsibleParty.socialSecurityNumber
        textFieldDriverLicense.text = patient.responsibleParty.driverLicense
        textFieldWorkPhone.text = patient.responsibleParty.workPhone
    }
    
    func saveValues() {
        self.view.endEditing(true)
        if radioButton.selected != nil  {
            patient.responsibleParty.responsiblepartyType = radioButton.selected.tag
        }
        patient.responsibleParty.address = textFieldAddressLine.text!
        patient.responsibleParty.city = textFieldCity.text!
        patient.responsibleParty.state = textFieldState.text!
        patient.responsibleParty.zip = textFieldZipCode.text!
        patient.responsibleParty.phone = textFieldPhone.text!
        patient.responsibleParty.dateOfBirth = textFieldBirthDate.text!
        patient.responsibleParty.cellPhone = textFieldCellular.text!
        
        patient.responsibleParty.workPhoneExt = textFieldExt.text!
        patient.responsibleParty.socialSecurityNumber = textFieldSecurityNumber.text
        patient.responsibleParty.driverLicense = textFieldDriverLicense.text
        patient.responsibleParty.workPhone = textFieldWorkPhone.text
    }
    
    override func buttonBackAction() {
        saveValues()
        super.buttonBackAction()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    
    @IBAction func buttonActionNext(_ sender: AnyObject) {
        if let _ = findEmptyTextField() {
            self.showAlert("PLEASE ENTER ALL REQUIRED FIELDS")
        } else if !textFieldZipCode.text!.isZipCode {
            self.showAlert("PLEASE ENTER VALID ZIPCODE")
        } else if  !textFieldPhone.isEmpty && !textFieldPhone.text!.isPhoneNumber {
            self.showAlert("PLEASE ENTER VALID HOME PHONE")
        } else if !textFieldCellular.isEmpty && !textFieldCellular.text!.isPhoneNumber {
            self.showAlert("PLEASE ENTER VALID CELLULAR")
        } else if !textFieldWorkPhone.isEmpty && !textFieldWorkPhone.text!.isPhoneNumber {
            self.showAlert("PLEASE ENTER VALID WORK PHONE")
        } else if !textFieldExt.isEmpty && !textFieldExt.text!.isValidExt {
            self.showAlert("PLEASE ENTER VALID EXT")
        } else if !textFieldSecurityNumber.isEmpty && !textFieldSecurityNumber.text!.isSocialSecurityNumber {
            self.showAlert("PLEASE ENTER VALID SOCIAL SECURITY NUMBER")
        }  else if radioButton.selected == nil  {
            self.showAlert("PLEASE SELECT ANY OPTION")
        } else {
            saveValues()
            
            let newPatientStep3VC = self.storyboard?.instantiateViewController(withIdentifier: "kPatientRegistrationStep3VC") as! PatientRegistrationStep3ViewController
            newPatientStep3VC.patient = self.patient
            self.navigationController?.pushViewController(newPatientStep3VC, animated: true)
        }
    }
    
    func findEmptyTextField() -> UITextField? {
        let textFields = [textFieldAddressLine, textFieldCity, textFieldState, textFieldZipCode, textFieldBirthDate]
        for textField in textFields {
            if (textField?.isEmpty)! {
                return textField
            }
        }
        return nil
    }
}
