//
//  PatientRegistrationFormViewController.swift
//  WestgateSmiles
//
//  Created by Leojin Bose on 3/4/16.
//  Copyright © 2016 SRS. All rights reserved.
//

import UIKit

class PatientRegistrationFormViewController: MCViewController {

    
    @IBOutlet weak var labelFirstName: UILabel!
    @IBOutlet weak var labelLastName: UILabel!
    @IBOutlet weak var labelInitial: UILabel!
    @IBOutlet weak var radioButtonPatientType: RadioButton!
    @IBOutlet weak var labelPreferredName: UILabel!
    @IBOutlet weak var labelFormID: UILabel!
    @IBOutlet weak var labelChartID: UILabel!
    
    @IBOutlet weak var labelPatientCellular: UILabel!
    @IBOutlet weak var labelPatientAddress: UILabel!
    @IBOutlet weak var labelPatientCity: UILabel!
    @IBOutlet weak var labelPatientHomePhone: UILabel!
    @IBOutlet weak var labelPatientBirthDate: UILabel!
    @IBOutlet weak var labelPatientWorkPhone: UILabel!
    @IBOutlet weak var labelPatientExt: UILabel!
    @IBOutlet weak var labelPatientStateZip: UILabel!
    @IBOutlet weak var labelPatientEmail: UILabel!
    @IBOutlet weak var labelPatientAge: UILabel!
    @IBOutlet weak var labelPatientSSN: UILabel!
    @IBOutlet weak var labelPatientLicense: UILabel!
    @IBOutlet weak var labelPatientReceieveEmail: UIButton!
    @IBOutlet weak var labelPatientMedicaid: UILabel!
    @IBOutlet weak var labelPatientCarrierID: UILabel!
    @IBOutlet weak var labelPatientPrefPharmacy: UILabel!

    @IBOutlet weak var labelPatientPrefHyg: UILabel!
    @IBOutlet weak var labelPatientPrefDentist: UILabel!
    @IBOutlet weak var labelPatientEmployerID: UILabel!
    @IBOutlet weak var radioButtonStudentStatus: RadioButton!
    @IBOutlet weak var radioButtonMaritialStatus: RadioButton!
    @IBOutlet weak var radioButtonGender: RadioButton!
    @IBOutlet weak var radioButtonEmploymentStatus: RadioButton!
    
    
    @IBOutlet weak var labelRepCellular: UILabel!
    @IBOutlet weak var labelRepFirstName: UILabel!
    @IBOutlet weak var labelRepLastName: UILabel!
    @IBOutlet weak var labelRepInitial: UILabel!
    @IBOutlet weak var labelRepAddress: UILabel!
    @IBOutlet weak var labelRepCityState: UILabel!
    @IBOutlet weak var labelRepHomePhone: UILabel!
    @IBOutlet weak var labelRepBirthDate: UILabel!
    @IBOutlet weak var labelRepWorkPhone: UILabel!
    @IBOutlet weak var labelRepSSN: UILabel!
    @IBOutlet weak var labelRepExt: UILabel!
    @IBOutlet weak var labelRepLicence: UILabel!
    @IBOutlet weak var radioButtonPolicyHolderType: RadioButton!

    @IBOutlet var imageViewSignature: UIImageView!
    
    
    @IBOutlet weak var labelPrimaryName: UILabel!
    @IBOutlet weak var labelPrimarySSN: UILabel!
    @IBOutlet weak var labelPrimaryEmployer: UILabel!
    @IBOutlet weak var labelPrimaryAddress: UILabel!
    @IBOutlet weak var labelPrimaryCity1: UILabel!
    @IBOutlet weak var labelPrimaryCityState: UILabel!
    @IBOutlet weak var labelPrimaryRemBenefits: UILabel!
    @IBOutlet weak var labelPrimaryRemDeduct: UILabel!
    @IBOutlet weak var labelPrimaryBirthDate: UILabel!
    @IBOutlet weak var labelPrimaryCompany: UILabel!
    @IBOutlet weak var labelPrimaryAddress2: UILabel!
    @IBOutlet weak var labelPrimaryCity2: UILabel!
    @IBOutlet weak var labelPrimaryCityState2: UILabel!
    @IBOutlet weak var radioButtonPrimaryRelationship: RadioButton!
    
    
    @IBOutlet weak var labelSecondaryName: UILabel!
    @IBOutlet weak var labelSecondarySSN: UILabel!
    @IBOutlet weak var labelSecondaryEmployer: UILabel!
    @IBOutlet weak var labelSecondaryAddress: UILabel!
    @IBOutlet weak var labelSecondaryCity1: UILabel!
    @IBOutlet weak var labelSecondaryCityState: UILabel!
    @IBOutlet weak var labelSecondaryRemBenefits: UILabel!
    @IBOutlet weak var labelSecondaryRemDeduct: UILabel!
    @IBOutlet weak var labelSecondaryBirthDate: UILabel!
    @IBOutlet weak var labelSecondaryCompany: UILabel!
    @IBOutlet weak var labelSecondaryAddress2: UILabel!
    @IBOutlet weak var labelSecondaryCity2: UILabel!
    @IBOutlet weak var labelSecondaryCityState2: UILabel!
    @IBOutlet weak var radioButtonSecondaryRelationship: RadioButton!
    
    
//    @IBOutlet weak var sec3LabelCardNo: UILabel!
//    @IBOutlet weak var sec3LabelCardType: UILabel!
//    @IBOutlet weak var sec3LabelCardExp: UILabel!
//    @IBOutlet weak var sec3LabelCellPhone: UILabel!
//    @IBOutlet weak var sec3LabelOtherAddre: UILabel!
//    @IBOutlet weak var sec3LabelFreqProphy: UILabel!
//    @IBOutlet weak var sec3LabelFreqPerioMaint: UILabel!
//    @IBOutlet weak var sec3LabelFreqSRP: UILabel!

    @IBOutlet weak var labelReference : UILabel!
    @IBOutlet weak var labelPreviousDentist : UILabel!
    @IBOutlet weak var labelEmergencyName : UILabel!
    @IBOutlet weak var labelEmergencyPhone : UILabel!
    
    @IBOutlet weak var PatientName: UILabel!
    
    @IBOutlet weak var labelDate: UILabel!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        
        PatientName.text = patient.fullName
        labelDate.text = patient.dateToday
        //PATIENT DETAILS
        labelFirstName.text = patient.firstName
        labelLastName.text = patient.lastName
        labelPreferredName.text = patient.prefferedName
        
        labelFormID.text = patient.formID
        imageViewSignature.image = patient.signature1
        labelInitial.text = patient.initial
        
        labelPatientAddress.text = patient.address
        labelPatientCity.text = patient.city
        labelPatientHomePhone.text = patient.phone == nil ? "" : patient.phone
        labelPatientBirthDate.text = patient.dateOfBirth
        labelPatientStateZip.text = patient.state + ", " + patient.zip

        labelPatientCellular.text = patient.cellPhone!
        
        let forms = patient.selectedForms.filter { (formObj) -> Bool in
            return formObj.formTitle == kNewPatientSignInForm || formObj.formTitle == kPatientSignInForm
            }
        
        if forms.count > 0 {
            let form = forms[0]
            labelChartID.text = form.toothNumbers
        }
        
      
        labelPatientAge.text = "\(patient.patientAge!)"
        
        labelPatientWorkPhone.text = patient.workPhone
        labelPatientExt.text = patient.workPhoneExt
        labelPatientEmail.text = patient.email
        labelPatientSSN.text = patient.socialSecurityNumber == nil ? "" : patient.socialSecurityNumber.socialSecurityNumber
        
        labelPatientLicense.text = patient.driverLicense
        
        labelPatientMedicaid.text = patient.medicaidID
        labelPatientCarrierID.text = patient.carrierID
        labelPatientPrefPharmacy.text = patient.prefPharm

        labelPatientPrefHyg.text = patient.preHygen
        labelPatientPrefDentist.text = patient.prefDentist
        labelPatientEmployerID.text = patient.employerID
        labelPatientReceieveEmail.isSelected = patient.receieveEmail
        
        radioButtonStudentStatus.isSelected = patient.studentStaus == nil ? false : patient.studentStaus == 1
        radioButtonGender.isSelected = patient.gender.index == 1
        
        for button in radioButtonMaritialStatus.groupButtons {
            if (button as AnyObject).tag == patient.maritalStatus.index {
                (button as! UIButton).isSelected = true
                break;
            }
        }

        for button in radioButtonEmploymentStatus.groupButtons {
            if (button as AnyObject).tag == patient.employeeStatus {
                (button as! UIButton).isSelected = true
                break;
            }
        }
        
        //RESPONSIBLE PARTY
        
        
        if let responsibleParty = self.patient.responsibleParty {
            
            radioButtonPatientType.setSelectedWithTag(patient.patientPolicyButton)
            labelRepCellular.text = patient.patientPolicyButton == 1 ? "" : responsibleParty.cellPhone
            labelRepFirstName.text = patient.patientPolicyButton == 1 ? "" : responsibleParty.firstName
            labelRepLastName.text = patient.patientPolicyButton == 1 ? "" : responsibleParty.lastName
            labelRepAddress.text = patient.patientPolicyButton == 1 ? "" : responsibleParty.address
            labelRepCityState.text = patient.patientPolicyButton == 1 ? "" : getAddressString(nil, city: responsibleParty.city, state: responsibleParty.state, zip: responsibleParty.zip)
            labelRepHomePhone.text = patient.patientPolicyButton == 1 ? "" : responsibleParty.phone
            labelRepBirthDate.text = patient.patientPolicyButton == 1 ? "" : responsibleParty.dateOfBirth
            labelRepInitial.text = patient.patientPolicyButton == 1 ? "" : responsibleParty.initial
            labelRepWorkPhone.text = patient.patientPolicyButton == 1 ? "" : responsibleParty.workPhone
            labelRepExt.text = patient.patientPolicyButton == 1 ? "" : responsibleParty.workPhoneExt
            labelRepSSN.text = patient.patientPolicyButton == 1 ? "" : responsibleParty.socialSecurityNumber
            labelRepLicence.text = patient.patientPolicyButton == 1 ? "" : responsibleParty.driverLicense
            if patient.patientPolicyButton == 2 {
                radioButtonPolicyHolderType.setSelectedWithTag(patient.responsibleParty.responsiblepartyType)
            } else {
                radioButtonPolicyHolderType.deselectAllButtons()
            }
        } else {
            radioButtonPatientType.setSelectedWithTag(patient.patientPolicyButton)
        }
        
        
        //PRIMARY INSURANCE
        if let primaryInsurance = self.patient.primaryInsurance {
            if let insuredName = primaryInsurance.name {
                labelPrimaryName.text = insuredName
            }
            if let insuredSSN = primaryInsurance.insuredSSN {
                labelPrimarySSN.text = insuredSSN.socialSecurityNumber
            }
            if let employerName = primaryInsurance.employerName {
                labelPrimaryEmployer.text = employerName
            }
            if let address = primaryInsurance.addressLine1 {
                labelPrimaryAddress.text = address
            }
            if let city = primaryInsurance.city1 {
                labelPrimaryCity1.text = city
            }
            labelPrimaryCityState.text = getAddressString(nil, city: nil, state: primaryInsurance.state1, zip: primaryInsurance.zipCode1)
            
            if let insuredName = primaryInsurance.remBeneFits {
                labelPrimaryRemBenefits.text = insuredName
            }
            if let remDeduct = primaryInsurance.remDeduct {
                labelPrimaryRemDeduct.text = remDeduct
            }
            if let companyName = primaryInsurance.insuranceCompany {
                labelPrimaryCompany.text = companyName
            }
            if let address2 = primaryInsurance.addressLine2 {
                labelPrimaryAddress2.text = address2
            }
            if let city = primaryInsurance.city2 {
                labelPrimaryCity2.text = city
            }
            labelPrimaryCityState2.text = getAddressString(nil, city: nil, state: primaryInsurance.state2, zip: primaryInsurance.zipCode2)
            
            labelPrimaryBirthDate.text = primaryInsurance.birthDate
            
            radioButtonPrimaryRelationship.setSelectedWithTag(primaryInsurance.typeInsured == nil ? 0 : primaryInsurance.typeInsured!)
//            for button in radioButtonPrimaryRelationship.groupButtons {
//                if button.tag == patient.primaryInsurance.typeInsured {
//                    (button as! UIButton).selected = true
//                    break;
//                }
//            }
        }
        
        
        //SECONDARY INSURANCE
        if let secondaryInsurance = patient.secondaryInsurance {
            if let insuredName = secondaryInsurance.name {
                labelSecondaryName.text = insuredName
            }
            if let insuredSSN = secondaryInsurance.insuredSSN {
                labelSecondarySSN.text = insuredSSN.socialSecurityNumber
            }
            if let employerName = secondaryInsurance.employerName {
                labelSecondaryEmployer.text = employerName
            }
            if let address = secondaryInsurance.addressLine1 {
                labelSecondaryAddress.text = address
            }
            if let city = secondaryInsurance.city1 {
                labelSecondaryCity1.text = city
            }
            labelSecondaryCityState.text = getAddressString(nil, city: nil, state: secondaryInsurance.state1, zip: secondaryInsurance.zipCode1)
            
            if let insuredName = secondaryInsurance.remBeneFits {
                labelSecondaryRemBenefits.text = insuredName
            }
            if let remDeduct = secondaryInsurance.remDeduct {
                labelSecondaryRemDeduct.text = remDeduct
            }
            if let companyName = secondaryInsurance.insuranceCompany {
                labelSecondaryCompany.text = companyName
            }
            if let address2 = secondaryInsurance.addressLine2 {
                labelSecondaryAddress2.text = address2
            }
            if let city = secondaryInsurance.city2 {
                labelSecondaryCity2.text = city
            }
            labelSecondaryCityState2.text = getAddressString(nil, city: nil, state: secondaryInsurance.state2, zip: secondaryInsurance.zipCode2)
            
            labelSecondaryBirthDate.text = secondaryInsurance.birthDate
            
            radioButtonSecondaryRelationship.setSelectedWithTag(secondaryInsurance.typeInsured == nil ? 0 : secondaryInsurance.typeInsured!)
//            for button in radioButtonSecondaryRelationship.groupButtons {
//                if button.tag == patient.secondaryInsurance.typeInsured {
//                    (button as! UIButton).selected = true
//                    break;
//                }
//            }
        }
//        sec3LabelCardNo.text = patient.Sec3cardNumber == nil ? "" : patient.Sec3cardNumber
//        sec3LabelCardType.text = patient.Sec3cardType == nil ? "" : patient.Sec3cardType
//        sec3LabelCardExp.text = patient.Sec3expiryDate == nil ? "" : patient.Sec3expiryDate
//        sec3LabelCellPhone.text = patient.Sec3CellPhone == nil ? "" : patient.Sec3CellPhone
//        sec3LabelOtherAddre.text = patient.Sec3OtherAddress == nil ? "" : patient.Sec3OtherAddress
//        sec3LabelFreqProphy.text = patient.Sec3FreqProphy == nil ? "" : patient.Sec3FreqProphy
//        sec3LabelFreqPerioMaint.text = patient.Sec3FreqPerioMaint == nil ? "" : patient.Sec3FreqPerioMaint
//        sec3LabelFreqSRP.text = patient.Sec3FreqSrp == nil ? "" : patient.Sec3FreqSrp    
        
        labelReference.text = patient.referredBy
        labelPreviousDentist.text = patient.previousDoctor
        labelEmergencyName.text = patient.emergencyContactName
        labelEmergencyPhone.text = patient.emergencyContactPhoneNumber
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    func getAddressString(_ address: String!, city: String!, state: String!, zip: String!) -> String {
        var addressString = [String]()
        
        if address != nil && address.characters.count > 0 {
            addressString.append(address)
        }
        if city != nil && city.characters.count > 0 {
            addressString.append(city)
        }
        if state != nil && state.characters.count > 0 {
            addressString.append(state)
        }
        if zip != nil && zip.characters.count > 0 {
            addressString.append(zip)
        }
        
        return (addressString as NSArray).componentsJoined(by: ", ")
    }
    
    override func onSubmitButtonPressed() {
        #if AUTO
            if !Reachability.isConnectedToNetwork() {
                let alertController = UIAlertController(title: kAppName, message: "Your device is not connected to internet. Please go to settings to connect.", preferredStyle: UIAlertControllerStyle.alert)
                let alertOkAction = UIAlertAction(title: "Settings", style: UIAlertActionStyle.destructive) { (action) -> Void in
                    let settingsUrl = NSURL(string: UIApplicationOpenSettingsURLString)
                    if let url = settingsUrl {
                        UIApplication.shared.openURL(url as URL)
                    }
                }
                let alertCancelAction = UIAlertAction(title: "Cancel", style: UIAlertActionStyle.destructive) { (action) -> Void in
                    
                }
                alertController.addAction(alertOkAction)
                alertController.addAction(alertCancelAction)
                self.present(alertController, animated: true, completion: nil)
                return
            }
            BRProgressHUD.show()
            ServiceManager.sendPatientDetails(patient: patient, completion: { (success, error) in
                BRProgressHUD.hide()
                if success {
                    super.onSubmitButtonPressed()
                } else {
                    self.showAlert(error!.localizedDescription.uppercased())
                }
            })
        #else
            super.onSubmitButtonPressed()
        #endif
    }
}
