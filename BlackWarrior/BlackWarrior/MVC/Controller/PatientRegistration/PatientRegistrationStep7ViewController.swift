//
//  PatientRegistrationStep7ViewController.swift
//  WestgateSmiles
//
//  Created by Leojin Bose on 3/4/16.
//  Copyright © 2016 SRS. All rights reserved.
//

import UIKit

class PatientRegistrationStep7ViewController: MCViewController {
    
    @IBOutlet weak var dropDownRelation: BRDropDown!
    @IBOutlet weak var textFieldAddressLine: MCTextField!
    @IBOutlet weak var textFieldState: MCTextField!
    @IBOutlet weak var textFieldCity: MCTextField!
    @IBOutlet weak var textFieldZipCode: MCTextField!
    @IBOutlet weak var textFieldInsuredName: MCTextField!
    @IBOutlet weak var textFieldSecurityNumber: MCTextField!
    @IBOutlet weak var textFieldEmployerName: MCTextField!
    @IBOutlet weak var textFieldDOB: MCTextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        textFieldSecurityNumber.textFormat = .socialSecurity
        textFieldZipCode.textFormat = .zipcode
        textFieldState.textFormat = .state
        textFieldDOB.textFormat = .dateIn1980
        
        dropDownRelation.items = ["SELF", "SPOUSE", "CHILD", "OTHER"]
        dropDownRelation.placeholder = "-- INSURED RELATIONSHIP --"
        dropDownRelation.delegate = self
        loadValues()
    }
    
    func loadValues() {
        if patient.secondaryInsurance == nil {
            patient.secondaryInsurance = Insurance()
        }
        
        dropDownRelation.selectedIndex = patient.secondaryInsurance.typeInsured == nil ? 0 : patient.secondaryInsurance.typeInsured!
        
        textFieldInsuredName.text = patient.secondaryInsurance.name
        textFieldSecurityNumber.text = patient.secondaryInsurance.insuredSSN
        textFieldAddressLine.text = patient.secondaryInsurance.addressLine1
        textFieldState.text = patient.secondaryInsurance.state1
        textFieldCity.text = patient.secondaryInsurance.city1
        textFieldZipCode.text = patient.secondaryInsurance.zipCode1
        textFieldEmployerName.text = patient.secondaryInsurance.employerName
        textFieldDOB.text = patient.secondaryInsurance.birthDate
    }
    
    func saveValues() {
        self.view.endEditing(true)
        dropDownRelation.selected = false
        
        patient.secondaryInsurance.typeInsured = dropDownRelation.selectedIndex
        
        patient.secondaryInsurance.name = textFieldInsuredName.text!
        patient.secondaryInsurance.insuredSSN = textFieldSecurityNumber.text!
        patient.secondaryInsurance.addressLine1 = textFieldAddressLine.text!
        patient.secondaryInsurance.state1 = textFieldState.text!
        patient.secondaryInsurance.city1 = textFieldCity.text!
        patient.secondaryInsurance.zipCode1 = textFieldZipCode.text!
        patient.secondaryInsurance.employerName = textFieldEmployerName.text!
        patient.secondaryInsurance.birthDate = textFieldDOB.text!
    }
    
    override func buttonBackAction() {
        saveValues()
        super.buttonBackAction()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        super.touchesBegan(touches, with: event)
        dropDownRelation.selected = false
    }

    
    @IBAction func buttonActionNext(_ sender: AnyObject) {
        if !textFieldSecurityNumber.isEmpty && !textFieldSecurityNumber.text!.isSocialSecurityNumber {
            self.showAlert("PLEASE ENTER VALID SOCIAL SECURITY NUMBER")
        } else if !textFieldZipCode.isEmpty && !textFieldZipCode.text!.isZipCode {
            self.showAlert("PLEASE ENTER VALID ZIPCODE")
        } else {
            saveValues()
            let newPatientStep8VC = self.storyboard?.instantiateViewController(withIdentifier: "kPatientRegistrationStep8VC") as! PatientRegistrationStep8ViewController
            newPatientStep8VC.patient = self.patient
            self.navigationController?.pushViewController(newPatientStep8VC, animated: true)
        }
    }
    
}

extension PatientRegistrationStep7ViewController : BRDropDownDelegate{
    func dropDown(_ dropDown: BRDropDown, selectedAtIndex index: Int, selectedOption option: String?) {
        if option != nil && option == "SELF"{
            textFieldInsuredName.text = patient.fullName
            textFieldSecurityNumber.text = patient.socialSecurityNumber == "N/A" ? "" : patient.socialSecurityNumber
            textFieldDOB.text = patient.dateOfBirth
        }else{
            textFieldInsuredName.text = ""
            textFieldSecurityNumber.text = ""
            textFieldDOB.text = ""
            textFieldAddressLine.text = ""
            textFieldCity.text = ""
            textFieldZipCode.text = ""
            textFieldEmployerName.text = ""
        
        }
        
    }
    
}
