//
//  PatientRegistrationStep3ViewController.swift
//  WestgateSmiles
//
//  Created by Leojin Bose on 3/4/16.
//  Copyright © 2016 SRS. All rights reserved.
//

import UIKit

class PatientRegistrationStep3ViewController: MCViewController {
    
    @IBOutlet weak var radioButtonGender: RadioButton!
    @IBOutlet weak var dropDownMarital: BRDropDown!
    
    @IBOutlet weak var textFieldAddressLine: MCTextField!
    @IBOutlet weak var textFieldState: MCTextField!
    @IBOutlet weak var textFieldCity: MCTextField!
    @IBOutlet weak var textFieldZipCode: MCTextField!
    @IBOutlet weak var textFieldPhone: MCTextField!
    @IBOutlet weak var textFieldSecurityNumber: MCTextField!
    @IBOutlet weak var textFieldWorkPhone: MCTextField!
    @IBOutlet weak var textFieldExt: MCTextField!
    @IBOutlet weak var textFieldDriverLicense: MCTextField!
    @IBOutlet weak var textFieldCellular: MCTextField!
    
    @IBOutlet var radioPatientInformation: RadioButton!
    @IBOutlet var radiobuttonNo: RadioButton!
    @IBOutlet var labelhide: UILabel!

    override func viewDidLoad() {
        super.viewDidLoad()
        
        textFieldZipCode.textFormat = .zipcode
        textFieldSecurityNumber.textFormat = .socialSecurity
        textFieldPhone.textFormat = .phone
        textFieldWorkPhone.textFormat = .phone
        textFieldCellular.textFormat = .phone
        textFieldExt.textFormat = .extensionCode
        textFieldState.textFormat = .state
        
        if patient.patientPolicyButton == 1 {
            labelhide.isHidden = true
            radioPatientInformation.isHidden = true
            radiobuttonNo.isHidden = true
        }else {
            labelhide.isHidden = false
            radioPatientInformation.isHidden = false
            radiobuttonNo.isHidden = false
        }
        
        dropDownMarital.items = ["Single", "Married", "Widowed", "Divorced", "Seperated"]
        dropDownMarital.placeholder = "-- MARITAL STATUS * --"
        
        
        loadValues()
    }
    
    func loadValues() {
        
        textFieldDriverLicense.text = patient.driverLicense
        textFieldExt.text = patient.workPhoneExt
        
        
        func loadVal() {
            textFieldAddressLine.text = patient.address
            textFieldCity.text = patient.city
            if patient.state != nil {
                textFieldState.text = patient.state
            }
            textFieldPhone.text = patient.phone
            textFieldZipCode.text = patient.zip
            if let gender = patient.gender {
                radioButtonGender.setSelectedWithTag(gender.index)
            }
            textFieldCellular.text = patient.cellPhone
            textFieldSecurityNumber.text = patient.socialSecurityNumber
            textFieldWorkPhone.text = patient.workPhone
            if let status = patient.maritalStatus {
                dropDownMarital.selectedIndex = status.index
            }
        }
        
        
        #if AUTO
            if let patientDetails = patient.patientDetails {
                if let gender = patient.gender {
                    radioButtonGender.setSelectedWithTag(gender.index)
                } else if let gender = patientDetails.gender {
                    radioButtonGender.setSelectedWithTag(gender)
                }
                if let maritalStatus = patient.maritalStatus {
                    dropDownMarital.selectedIndex = maritalStatus.index
                } else if let maritalStatus = patientDetails.maritalStatus {
                    dropDownMarital.selectedIndex = maritalStatus.index
                }
                textFieldCity.text = patient.city != nil ? patient.city : patientDetails.city
                textFieldState.text = patient.state != nil ? patient.state : patientDetails.state
                textFieldZipCode.text = patient.zip != nil ? patient.zip : patientDetails.zipCode
                textFieldAddressLine.text = patient.address != nil ? patient.address : patientDetails.address
                textFieldPhone.text = patient.phone != nil ? patient.phone : patientDetails.homePhone
                textFieldCellular.text = patient.cellPhone != nil ? patient.cellPhone : patientDetails.cellPhone
                textFieldWorkPhone.text = patient.workPhone != nil ? patient.workPhone : patientDetails.workPhone
                textFieldSecurityNumber.text = patient.socialSecurityNumber != nil ? patient.socialSecurityNumber : patientDetails.socialSecurityNumber
            } else {
                loadVal()
            }
        #else
            loadVal()
        #endif
    }
    
    func saveValues() {
        self.view.endEditing(true)
        dropDownMarital.selected = false
        
        patient.address = textFieldAddressLine.text!
        patient.city = textFieldCity.text!
        patient.state = textFieldState.text!
        patient.phone = textFieldPhone.text!
        patient.zip = textFieldZipCode.text!
        
        if let selected = radioButtonGender.selected {
            patient.gender = Gender(value: selected.tag)
        }
        patient.cellPhone = textFieldCellular.text!
        
        patient.driverLicense = textFieldDriverLicense.text!
        
        patient.workPhoneExt = textFieldExt.text!
        patient.socialSecurityNumber = textFieldSecurityNumber.text!
        patient.workPhone = textFieldWorkPhone.text!
        if let selected = dropDownMarital?.selectedIndex {
            patient.maritalStatus = MaritalStatus(value: selected)
        }
    }
    
    override func buttonBackAction() {
        saveValues()
        super.buttonBackAction()
    }
    
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        super.touchesBegan(touches, with: event)
        dropDownMarital.selected = false
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
   
    @IBAction func radioButtonAction(_ sender: RadioButton) {
        
        if sender.selected.tag == 1 {
            textFieldAddressLine.text = patient.responsibleParty.address
            textFieldCity.text = patient.responsibleParty.city
            textFieldState.text = patient.responsibleParty.state
            textFieldZipCode.text = patient.responsibleParty.zip
            textFieldPhone.text = patient.responsibleParty.phone
            textFieldCellular.text = patient.responsibleParty.cellPhone
            textFieldWorkPhone.text = patient.responsibleParty.workPhone
            textFieldExt.text = patient.responsibleParty.workPhoneExt
            textFieldDriverLicense.text = patient.responsibleParty.driverLicense
            textFieldSecurityNumber.text = patient.responsibleParty.socialSecurityNumber == nil ? "" : patient.responsibleParty.socialSecurityNumber
        } else {
           textFieldAddressLine.text = ""
            textFieldCity.text = ""
            textFieldState.text = "IL"
            textFieldZipCode.text = ""
            textFieldPhone.text = ""
            textFieldCellular.text = ""
            textFieldWorkPhone.text = ""
            textFieldExt.text = ""
            textFieldSecurityNumber.text = ""
            textFieldDriverLicense.text = ""
        }
    }
    
    @IBAction func buttonActionNext(_ sender: AnyObject) {
        if let _ = findEmptyTextField() {
            self.showAlert("PLEASE ENTER ALL REQUIRED FIELDS")
        } else if !textFieldZipCode.text!.isZipCode {
            self.showAlert("PLEASE ENTER VALID ZIPCODE")
        } else if !textFieldPhone.isEmpty && !textFieldPhone.text!.isPhoneNumber {
            self.showAlert("PLEASE ENTER VALID HOME PHONE")
        }  else if !textFieldCellular.isEmpty && !textFieldCellular.text!.isPhoneNumber {
            self.showAlert("PLEASE ENTER VALID CELLULAR")
        } else if !textFieldWorkPhone.isEmpty && !textFieldWorkPhone.text!.isPhoneNumber {
            self.showAlert("PLEASE ENTER VALID WORK PHONE")
        } else if !textFieldExt.isEmpty && !textFieldExt.text!.isValidExt {
            self.showAlert("PLEASE ENTER VALID EXT")
        } else if !textFieldSecurityNumber.isEmpty && !textFieldSecurityNumber.text!.isSocialSecurityNumber {
            self.showAlert("PLEASE ENTER VALID SOCIAL SECURITY NUMBER")
        }  else if radioButtonGender.selected == nil  {
            self.showAlert("PLEASE SELECT GENDER")
        } else if dropDownMarital.selectedIndex == 0 {
            self.showAlert("PLEASE SELECT YOUR MARITAL STATUS")
        } else {
            saveValues()
            
            let newPatientStep4VC = self.storyboard?.instantiateViewController(withIdentifier: "kPatientRegistrationStep4VC") as! PatientRegistrationStep4ViewController
            newPatientStep4VC.patient = self.patient
            self.navigationController?.pushViewController(newPatientStep4VC, animated: true)
            
        }
    }


    @IBAction func buttonReceiveEmails(_ sender: UIButton) {
        sender.isSelected = !sender.isSelected
    }

    func findEmptyTextField() -> UITextField? {
        let textFields = [textFieldAddressLine, textFieldCity, textFieldState, textFieldZipCode]
        for textField in textFields {
            if (textField?.isEmpty)! {
                return textField
            }
        }
        return nil
    }
}
