//
//  PatientRegistrationStep5ViewController.swift
//  WestgateSmiles
//
//  Created by Leojin Bose on 3/4/16.
//  Copyright © 2016 SRS. All rights reserved.
//

import UIKit

class PatientRegistrationStep5ViewController: MCViewController {

//    @IBOutlet weak var radioButton: RadioButton!
    @IBOutlet weak var dropDownRelation: BRDropDown!
    @IBOutlet weak var textFieldAddressLine: MCTextField!
    @IBOutlet weak var textFieldState: MCTextField!
    @IBOutlet weak var textFieldCity: MCTextField!
    @IBOutlet weak var textFieldZipCode: MCTextField!
    @IBOutlet weak var textFieldInsuredName: MCTextField!
    @IBOutlet weak var textFieldSecurityNumber: MCTextField!
    @IBOutlet weak var textFieldEmployerName: MCTextField!
    @IBOutlet weak var textFieldDOB: MCTextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        textFieldZipCode.textFormat = .zipcode
        textFieldSecurityNumber.textFormat = .socialSecurity
        textFieldState.textFormat = .state
        textFieldDOB.textFormat = .dateIn1980
        
        dropDownRelation.items = ["SELF", "SPOUSE", "CHILD", "OTHER"]
        dropDownRelation.delegate = self
        dropDownRelation.placeholder = "-- INSURED RELATIONSHIP --"

        loadValues()
    }
    
    func loadValues() {
        if patient.primaryInsurance == nil {
            patient.primaryInsurance = Insurance()
        }
        
        dropDownRelation.selectedIndex = patient.primaryInsurance.typeInsured == nil ? 0 : patient.primaryInsurance.typeInsured!
        
        textFieldInsuredName.text = patient.primaryInsurance.name
        textFieldSecurityNumber.text = patient.primaryInsurance.insuredSSN
        textFieldAddressLine.text = patient.primaryInsurance.addressLine1
        textFieldState.text = patient.primaryInsurance.state1
        textFieldCity.text = patient.primaryInsurance.city1
        textFieldZipCode.text = patient.primaryInsurance.zipCode1
        textFieldEmployerName.text = patient.primaryInsurance.employerName
        textFieldDOB.text = patient.primaryInsurance.birthDate
    }
    
    func saveValues() {
        self.view.endEditing(true)
        dropDownRelation.selected = false
        
        patient.primaryInsurance.typeInsured = dropDownRelation.selectedIndex
        
        patient.primaryInsurance.name = textFieldInsuredName.text!
        patient.primaryInsurance.insuredSSN = textFieldSecurityNumber.text!
        patient.primaryInsurance.addressLine1 = textFieldAddressLine.text!
        patient.primaryInsurance.state1 = textFieldState.text!
        patient.primaryInsurance.city1 = textFieldCity.text!
        patient.primaryInsurance.zipCode1 = textFieldZipCode.text!
        patient.primaryInsurance.employerName = textFieldEmployerName.text!
        patient.primaryInsurance.birthDate = textFieldDOB.text
    }
    
    override func buttonBackAction() {
        saveValues()
        super.buttonBackAction()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        super.touchesBegan(touches, with: event)
        dropDownRelation.selected = false
    }
    
    @IBAction func buttonActionNext(_ sender: AnyObject) {
        if !textFieldSecurityNumber.isEmpty && !textFieldSecurityNumber.text!.isSocialSecurityNumber {
            self.showAlert("PLEASE ENTER VALID SOCIAL SECURITY NUMBER")
        } else if !textFieldZipCode.isEmpty && !textFieldZipCode.text!.isZipCode {
            self.showAlert("PLEASE ENTER VALID ZIPCODE")
        } else {
            saveValues()
            
            let newPatientStep6VC = self.storyboard?.instantiateViewController(withIdentifier: "kPatientRegistrationStep6VC") as! PatientRegistrationStep6ViewController
            newPatientStep6VC.patient = self.patient
            self.navigationController?.pushViewController(newPatientStep6VC, animated: true)
        }
    }
}

extension PatientRegistrationStep5ViewController : BRDropDownDelegate {
    
    func dropDown(_ dropDown: BRDropDown, selectedAtIndex index: Int, selectedOption option: String?) {
        if option != nil && option == "SELF" {
            textFieldInsuredName.text = patient.fullName
            textFieldSecurityNumber.text = patient.socialSecurityNumber == "N/A" ? "" : patient.socialSecurityNumber
            textFieldDOB.text = patient.dateOfBirth
        } else {
            textFieldInsuredName.text = ""
            textFieldSecurityNumber.text = ""
            textFieldDOB.text = ""
            textFieldAddressLine.text = ""
            textFieldCity.text = ""
            textFieldZipCode.text = ""
            textFieldEmployerName.text = ""
            
            
        }
    }
}
