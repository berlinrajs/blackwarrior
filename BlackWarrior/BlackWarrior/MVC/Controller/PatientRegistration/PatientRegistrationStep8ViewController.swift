//
//  PatientRegistrationStep8ViewController.swift
//  WestgateSmiles
//
//  Created by Leojin Bose on 3/4/16.
//  Copyright © 2016 SRS. All rights reserved.
//

import UIKit

class PatientRegistrationStep8ViewController: MCViewController {
    
    @IBOutlet weak var textFieldRemDeduct: MCTextField!
    @IBOutlet weak var textFieldRemBenefits: MCTextField!
    
    @IBOutlet weak var textFieldInsuranceCompany: MCTextField!
    @IBOutlet weak var textFieldAddressLine2: MCTextField!
    @IBOutlet weak var textFieldState2: MCTextField!
    @IBOutlet weak var textFieldCity2: MCTextField!
    @IBOutlet weak var textFieldZipCode2: MCTextField!
    
    @IBOutlet weak var PatientSingName: UILabel!
    @IBOutlet var imageViewSignature: SignatureView!
    
    @IBOutlet var labelDate: DateLabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        labelDate.todayDate = patient.dateToday
        StateListView.addStateListForTextField(textFieldState2)
        textFieldRemDeduct.textFormat = .numbersWithoutValidation
        textFieldRemBenefits.textFormat = .numbersWithoutValidation
        textFieldZipCode2.textFormat = .zipcode
        PatientSingName.text = patient.fullName
        // Do any additional setup after loading the view.
        loadValues()
    }
    
    func loadValues() {
        textFieldRemDeduct.text = patient.secondaryInsurance.remDeduct
        textFieldRemBenefits.text = patient.secondaryInsurance.remBeneFits
        textFieldInsuranceCompany.text = patient.secondaryInsurance.insuranceCompany
        textFieldAddressLine2.text = patient.secondaryInsurance.addressLine2
        if patient.secondaryInsurance.state2 != nil {
            textFieldState2.text = patient.secondaryInsurance.state2
        }
        textFieldCity2.text = patient.secondaryInsurance.city2
        textFieldZipCode2.text = patient.secondaryInsurance.zipCode2
    }
    
    func saveValues() {
        self.view.endEditing(true)
        
        patient.secondaryInsurance.remDeduct = textFieldRemDeduct.text!
        patient.secondaryInsurance.remBeneFits = textFieldRemBenefits.text!
        patient.secondaryInsurance.insuranceCompany = textFieldInsuranceCompany.text!
        patient.secondaryInsurance.addressLine2 = textFieldAddressLine2.text!
        patient.secondaryInsurance.state2 = textFieldState2.text!
        patient.secondaryInsurance.city2 = textFieldCity2.text!
        patient.secondaryInsurance.zipCode2 = textFieldZipCode2.text!
    }
    
    override func buttonBackAction() {
        saveValues()
        super.buttonBackAction()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    
    @IBAction func buttonActionNext(_ sender: AnyObject) {
        if !textFieldZipCode2.isEmpty && !textFieldZipCode2.text!.isZipCode {
            self.showAlert("PLEASE ENTER VALID ZIPCODE")
        } else if !imageViewSignature.isSigned() {
            self.showAlert("PLEASE SIGN THE FORM")
        } else if !labelDate.dateTapped{
            self.showAlert("PLEASE SELECT THE DATE")
        } else{
            let newPatientFormVC = self.storyboard?.instantiateViewController(withIdentifier: "kPatientRegistrationFormVC") as! PatientRegistrationFormViewController
            saveValues()
            patient.signature1 = imageViewSignature.image
            newPatientFormVC.patient = self.patient
            self.navigationController?.pushViewController(newPatientFormVC, animated: true)
        }
    }
}

extension PatientRegistrationStep8ViewController : UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if textField == textFieldZipCode2 {
            return textField.formatZipCode(range, string: string)
        }
        return true
    }
}
