//
//  PatientRegistrationStep4ViewController.swift
//  WestgateSmiles
//
//  Created by Leojin Bose on 3/4/16.
//  Copyright © 2016 SRS. All rights reserved.
//

import UIKit

class PatientRegistrationStep4ViewController: MCViewController {
    
    @IBOutlet weak var textFieldPrefHygn: MCTextField!
    @IBOutlet weak var textFieldPrefPharmacy: MCTextField!
    @IBOutlet weak var textFieldPrefDentist: MCTextField!
    @IBOutlet weak var textFieldCarrierID: MCTextField!
    @IBOutlet weak var textFieldMedicaidID: MCTextField!
    @IBOutlet weak var radioButtonEmploymentStatus: RadioButton!
    @IBOutlet weak var radioButtonStudentStatus: RadioButton!
    @IBOutlet weak var textFieldEmail: MCTextField!
    @IBOutlet weak var buttonSubscribe: RadioButton!
    @IBOutlet weak var textFieldEmployerID: MCTextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        textFieldEmail.textFormat = .email
        loadValues()
    }
    
    func loadValues() {
        #if AUTO
            if let patientDetails = patient.patientDetails, patient.email == nil {
                patient.email = patientDetails.email
            }
        #endif
        
        buttonSubscribe.isSelected = patient.receieveEmail != nil && patient.receieveEmail == true
        radioButtonEmploymentStatus.setSelectedWithTag(patient.employeeStatus == nil ? 0 : patient.employeeStatus!)
        radioButtonStudentStatus.setSelectedWithTag(patient.studentStaus == nil ? 0 : patient.studentStaus!)
        
        textFieldPrefHygn.text = patient.preHygen
        textFieldPrefPharmacy.text = patient.prefPharm
        textFieldPrefDentist.text = patient.prefDentist
        textFieldCarrierID.text = patient.carrierID
        textFieldMedicaidID.text = patient.medicaidID
        textFieldEmail.text = patient.email
        textFieldEmployerID.text = patient.employerID
    }
    
    func saveValues() {
        self.view.endEditing(true)
        
        patient.receieveEmail = buttonSubscribe.isSelected
        patient.employeeStatus = radioButtonEmploymentStatus.selected == nil ? 0 : radioButtonEmploymentStatus.selected.tag
        patient.studentStaus = radioButtonStudentStatus.selected == nil ? 0 : radioButtonStudentStatus.selected.tag
        
        patient.preHygen = textFieldPrefHygn.text!
        patient.prefPharm = textFieldPrefPharmacy.text!
        patient.prefDentist = textFieldPrefDentist.text!
        patient.carrierID = textFieldCarrierID.text!
        patient.medicaidID = textFieldMedicaidID.text!
        patient.email = textFieldEmail.text!
        patient.employerID = textFieldEmployerID.text!
    }
    
    override func buttonBackAction() {
        saveValues()
        super.buttonBackAction()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    
    @IBAction func buttonActionNext(_ sender: AnyObject) {
        if !textFieldEmail.isEmpty && !textFieldEmail.text!.isValidEmail {
            self.showAlert("PLEASE ENTER VALID EMAIL")
        } else {
            saveValues()

            let newPatientStep9VC = self.storyboard?.instantiateViewController(withIdentifier: "kPatientRegistrationStep9VC") as! PatientRegistrationStep9ViewController
            newPatientStep9VC.patient = self.patient
            self.navigationController?.pushViewController(newPatientStep9VC, animated: true)
        }

        
    }
    
    @IBAction func buttonReceiveEmails(_ sender: UIButton) {
        if !textFieldEmail.isEmpty && textFieldEmail.text!.isValidEmail {
            sender.isSelected = !sender.isSelected
        }
    }
}

extension PatientRegistrationStep4ViewController : UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if textField == textFieldEmail {
            let newRange = textField.text!.characters.index(textField.text!.startIndex, offsetBy: range.location)..<textField.text!.characters.index(textField.text!.startIndex, offsetBy: range.location + range.length)
            let newString = textField.text!.replacingCharacters(in: newRange, with: string)
            buttonSubscribe.isSelected = newString.isValidEmail
        }
        return true
    }
}
