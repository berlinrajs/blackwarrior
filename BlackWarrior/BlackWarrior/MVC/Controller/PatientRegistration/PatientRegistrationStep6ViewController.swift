//
//  PatientRegistrationStep6ViewController.swift
//  WestgateSmiles
//
//  Created by Leojin Bose on 3/4/16.
//  Copyright © 2016 SRS. All rights reserved.
//

import UIKit

class PatientRegistrationStep6ViewController: MCViewController {


    @IBOutlet weak var textFieldRemDeduct: MCTextField!
    @IBOutlet weak var textFieldRemBenefits: MCTextField!

    @IBOutlet weak var textFieldInsuranceCompany: MCTextField!
    @IBOutlet weak var textFieldAddressLine2: MCTextField!
    @IBOutlet weak var textFieldState2: MCTextField!
    @IBOutlet weak var textFieldCity2: MCTextField!
    @IBOutlet weak var textFieldZipCode2: MCTextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        textFieldZipCode2.textFormat = .zipcode
        textFieldState2.textFormat = .state
        textFieldRemDeduct.textFormat = .numbersWithoutValidation
        textFieldRemBenefits.textFormat = .numbersWithoutValidation
        loadValues()
    }
    
    func loadValues() {
        textFieldRemDeduct.text = patient.primaryInsurance.remDeduct
        textFieldRemBenefits.text = patient.primaryInsurance.remBeneFits
        textFieldInsuranceCompany.text = patient.primaryInsurance.insuranceCompany
        textFieldAddressLine2.text = patient.primaryInsurance.addressLine2
        if patient.primaryInsurance.state2 != nil {
            textFieldState2.text = patient.primaryInsurance.state2
        }
        textFieldCity2.text = patient.primaryInsurance.city2
        textFieldZipCode2.text = patient.primaryInsurance.zipCode2
    }
    
    func saveValues() {
        self.view.endEditing(true)
        patient.primaryInsurance.remDeduct = textFieldRemDeduct.text!
        patient.primaryInsurance.remBeneFits = textFieldRemBenefits.text!
        patient.primaryInsurance.insuranceCompany = textFieldInsuranceCompany.text!
        patient.primaryInsurance.addressLine2 = textFieldAddressLine2.text!
        patient.primaryInsurance.state2 = textFieldState2.text!
        patient.primaryInsurance.city2 = textFieldCity2.text!
        patient.primaryInsurance.zipCode2 = textFieldZipCode2.text!
    }
    
    override func buttonBackAction() {
        saveValues()
        super.buttonBackAction()
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    @IBAction func buttonActionNext(_ sender: AnyObject) {
        if !textFieldZipCode2.isEmpty && !textFieldZipCode2.text!.isZipCode {
            self.showAlert("PLEASE ENTER VALID ZIPCODE")
        } else {
            saveValues()
            let newPatientStep7VC = self.storyboard?.instantiateViewController(withIdentifier: "kPatientRegistrationStep7VC") as! PatientRegistrationStep7ViewController
            newPatientStep7VC.patient = self.patient
            self.navigationController?.pushViewController(newPatientStep7VC, animated: true)
        }
    }
}
