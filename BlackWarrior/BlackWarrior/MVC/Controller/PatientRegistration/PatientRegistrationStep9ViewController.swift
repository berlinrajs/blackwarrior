//
//  CreditCardAuthorizationStep3ViewController.swift
//  FusionDental
//
//  Created by Leojin Bose on 5/12/16.
//  Copyright © 2016 SRS. All rights reserved.
//

import UIKit

class PatientRegistrationStep9ViewController: MCViewController {
    
    @IBOutlet weak var textfieldReference : MCTextField!
    @IBOutlet weak var textfieldDentist : MCTextField!
    @IBOutlet weak var textfieldEmergencyName : MCTextField!
    @IBOutlet weak var textfieldEmergencyPhone : MCTextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        loadValues()
    }
    
    func loadValues() {
        textfieldEmergencyPhone.textFormat = .phone
        textfieldReference.text = patient.referredBy
        textfieldDentist.text = patient.previousDoctor
        textfieldEmergencyName.text = patient.emergencyContactName
        textfieldEmergencyPhone.text = patient.emergencyContactPhoneNumber
    }
    
    func saveValues() {
        self.view.endEditing(true)
        patient.referredBy = textfieldReference.isEmpty ? "" : textfieldReference.text!
        patient.previousDoctor = textfieldDentist.isEmpty ? "" : textfieldDentist.text!
        patient.emergencyContactName = textfieldEmergencyName.isEmpty ? "" : textfieldEmergencyName.text!
        patient.emergencyContactPhoneNumber = textfieldEmergencyPhone.isEmpty ? "" : textfieldEmergencyPhone.text!
        
    }
    
    override func buttonBackAction() {
        saveValues()
        super.buttonBackAction()
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    @IBAction func buttonActionNext(_ sender : AnyObject) {
        if !textfieldEmergencyPhone.isEmpty && !textfieldEmergencyPhone.text!.isPhoneNumber{
            self.showAlert("PLEASE ENTER THE VALID EMERGENCY CONTACT PHONE NUMBER")
        }else{
            saveValues()
            let newPatientStep5VC = self.storyboard?.instantiateViewController(withIdentifier: "kPatientRegistrationStep5VC") as! PatientRegistrationStep5ViewController
            newPatientStep5VC.patient = self.patient
            self.navigationController?.pushViewController(newPatientStep5VC, animated: true)

            
        }
        //        self.view.endEditing(true)
        //        if  !textFieldCellPhone.isEmpty && !textFieldCellPhone.text!.isPhoneNumber {
        //            self.showAlert("PLEASE ENTER VALID CELL PHONE")
        //        } else {
        //            saveValues()
        //
        //            if !textFieldCardNumber.isEmpty && !textFieldCardNumber.text!.isCreditCard && dropDownCard.selectedIndex != 3 {
        //                self.showAlert("PLEASE ENTER VALID CARD NUMBER")
        //            } else if !textFieldCardNumber.text!.isAmericanCreditCard && dropDownCard.selectedIndex == 3 {
        //                self.showAlert("PLEASE ENTER VALID CARD NUMBER")
        //            }  else {
        //                let newPatientStep5VC = self.storyboard?.instantiateViewControllerWithIdentifier("kPatientRegistrationStep5VC") as! PatientRegistrationStep5ViewController
        //                newPatientStep5VC.patient = self.patient
        //                self.navigationController?.pushViewController(newPatientStep5VC, animated: true)
        //            }
        //        }
    }
    
}


