//
//  PatientRegistrationStep1ViewController.swift
//  WestgateSmiles
//
//  Created by Leojin Bose on 3/2/16.
//  Copyright © 2016 SRS. All rights reserved.
//

import UIKit

class PatientRegistrationStep1ViewController: MCViewController {
    
    @IBOutlet weak var viewBackground: MCView!
    @IBOutlet weak var textFieldFirstName: MCTextField!
    @IBOutlet weak var textFieldPrefferedName: MCTextField!
    @IBOutlet weak var textFieldLastName: MCTextField!
    @IBOutlet weak var textFieldInitial: MCTextField!
    @IBOutlet weak var radioButton: RadioButton!
    @IBOutlet weak var textFieldID: MCTextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        textFieldID.textFormat = .numbersWithoutValidation
        textFieldInitial.textFormat = .middleInitial
        loadValues()
    }
    
    func loadValues() {
        radioButton.setSelectedWithTag(patient.patientPolicyButton == nil ? 0 : patient.patientPolicyButton)
        
        if radioButton.selected != nil && radioButton.selected.tag == 1 {
            let delayTime = DispatchTime.now() + Double(Int64(0.2 * Double(NSEC_PER_SEC))) / Double(NSEC_PER_SEC)
            DispatchQueue.main.asyncAfter(deadline: delayTime) {
                self.setInactive()
            }
        }
        
        textFieldPrefferedName.text = patient.prefferedName
        textFieldID.text = patient.formID
        if patient.responsibleParty == nil {
            patient.responsibleParty = MCPatient()
        }
        textFieldFirstName.text = patient.responsibleParty.firstName
        textFieldLastName.text = patient.responsibleParty.lastName
        textFieldInitial.text = patient.responsibleParty.initial
    }
    
    func saveValues() {
        patient.patientPolicyButton = radioButton.selected == nil ? 0 : radioButton.selected.tag
        
        patient.prefferedName = textFieldPrefferedName.text!
        patient.formID = textFieldID.text!
        
        patient.responsibleParty.firstName = textFieldFirstName.text!
        patient.responsibleParty.lastName = textFieldLastName.text!
        patient.responsibleParty.initial = textFieldInitial.text!
    }
    
    override func buttonBackAction() {
        saveValues()
        super.buttonBackAction()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func buttonActionNext(_ sender: AnyObject) {
        if radioButton.selected.tag == 1 {
            saveValues()
            let newPatientStep3VC = self.storyboard?.instantiateViewController(withIdentifier: "kPatientRegistrationStep3VC") as! PatientRegistrationStep3ViewController
            newPatientStep3VC.patient = self.patient
            self.navigationController?.pushViewController(newPatientStep3VC, animated: true)
        } else {
            if textFieldLastName.isEmpty || textFieldLastName.isEmpty{
                self.showAlert("PLEASE ENTER ALL REQUIRED FIELDS")
            } else {
                saveValues()
                let newPatientStep2VC = self.storyboard?.instantiateViewController(withIdentifier: "kPatientRegistrationStep2VC") as! PatientRegistrationStep2ViewController
                newPatientStep2VC.patient = self.patient
                self.navigationController?.pushViewController(newPatientStep2VC, animated: true)
            }
        }
    }
    
    func setInactive() {
        let label = viewBackground.subviews[0] as! UILabel
        label.textColor = UIColor.white.withAlphaComponent(0.3)
        viewBackground.layer.borderColor = UIColor.white.withAlphaComponent(0.3).cgColor
        textFieldFirstName.layer.borderColor = UIColor.white.withAlphaComponent(0.3).cgColor
        textFieldLastName.layer.borderColor = UIColor.white.withAlphaComponent(0.3).cgColor
        textFieldInitial.layer.borderColor = UIColor.white.withAlphaComponent(0.3).cgColor
        viewBackground.isUserInteractionEnabled = false
    }
    
    
    @IBAction func radioButtonAction(_ sender: RadioButton) {
        self.view.endEditing(true)
        if sender.tag == 2 {
            let label = viewBackground.subviews[0] as! UILabel
            label.textColor = UIColor.white
            viewBackground.layer.borderColor = UIColor.white.cgColor
            viewBackground.isUserInteractionEnabled = true
            textFieldFirstName.layer.borderColor = UIColor.white.cgColor
            textFieldLastName.layer.borderColor = UIColor.white.cgColor
            textFieldInitial.layer.borderColor = UIColor.white.cgColor
        } else {
            textFieldFirstName.text = ""
            textFieldLastName.text = ""
            textFieldInitial.text = ""
            setInactive()
        }
    }
    
    func findEmptyTextField() -> UITextField? {
        let textFields = [textFieldFirstName, textFieldLastName]
        for textField in textFields {
            if (textField?.isEmpty)! {
                return textField
            }
        }
        return nil
    }
}
