//
//  Periodontal1ViewController.swift
//  BlackWarrior
//
//  Created by Bala Murugan on 12/17/16.
//  Copyright © 2016 Bala Murugan. All rights reserved.
//

import UIKit

class Periodontal1ViewController: MCViewController {

    @IBOutlet weak var signaturePatient : SignatureView!
    @IBOutlet weak var labelDate : DateLabel!
    @IBOutlet weak var signatureWitness : SignatureView!
    @IBOutlet weak var labelDate1 : DateLabel!

    override func viewDidLoad() {
        super.viewDidLoad()

        labelDate.todayDate = patient.dateToday
        labelDate1.todayDate = patient.dateToday
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func onNextButtonPressed (withSender sender : UIButton){
        if !signaturePatient.isSigned() || !signatureWitness.isSigned(){
            self.showAlert("PLEASE SIGN THE FORM")
        }else if !labelDate.dateTapped || !labelDate1.dateTapped{
            self.showAlert("PLEASE SELECT THE DATE")
        }else{
            let periodontal = consentStoryBoard.instantiateViewController(withIdentifier: "PeriodontalFormVC") as! PeriodontalFormViewController
            periodontal.patient = self.patient
            periodontal.signPatient = signaturePatient.signatureImage()
            periodontal.signWitness = signatureWitness.signatureImage()
            self.navigationController?.pushViewController(periodontal, animated: true)
            
        }
    }

}
