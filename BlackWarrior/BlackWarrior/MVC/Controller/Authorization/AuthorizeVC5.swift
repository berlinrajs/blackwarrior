//
//  ApicoectomyVC.swift
//  SmileDesignDentalSpa
//
//  Created by APPLE on 12/5/16.
//  Copyright © 2016 srs. All rights reserved.
//

import UIKit

class AuthorizeVC5: MCViewController {

    @IBOutlet weak var sign1: SignatureView!
    @IBOutlet weak var date1: DateLabel!
    @IBOutlet weak var sign2: SignatureView!
    @IBOutlet weak var date2: DateLabel!
    
    @IBOutlet weak var textFieldRelation: MCTextField!
    override func viewDidLoad() {
        super.viewDidLoad()

     loadValues()
        date1.todayDate = patient.dateToday
        date2.todayDate = patient.dateToday
        // Do any additional setup after loading the view...
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction override func buttonBackAction() {
      saveValues()
        self.navigationController?.popViewController(animated: true)
    }
    
    func saveValues(){
    patient.relationshipToClientIfPersonalRep = textFieldRelation.text
    patient.signature1 = sign1.signatureImage()
    patient.signature2 = sign2.signatureImage()
    
    }
    
    func loadValues(){
      textFieldRelation.text = patient.relationshipToClientIfPersonalRep
    
    }

    @IBAction func doneAction(_ sender: AnyObject) {
        
        if !sign1.isSigned() || !sign2.isSigned(){
        self.showAlert("PLEASE SIGN THE FORM")
        }else if !date1.dateTapped || !date2.dateTapped{
        self.showAlert("PLEASE SELECT THE DATE")
        }else{
            saveValues()
          
            let nextVc = consentStoryBoard2.instantiateViewController(withIdentifier: "kAuthorizeVC6") as! AuthorizeVC6
            nextVc.patient = self.patient
            self.navigationController?.pushViewController(nextVc, animated: true)
        
        }
        
    }


}
