//
//  PatientSignInStep2VC.swift
//  MConsentForms
//
//  Created by Berlin Raj on 22/09/16.
//  Copyright © 2016 SRS Web Solutions. All rights reserved.
//

import UIKit

class AuthorizationVC4: MCViewController {

    @IBOutlet weak var textFieldMakeDisclosure: MCTextField!
    @IBOutlet weak var textFieldReceiveDisclosure: MCTextField!
    @IBOutlet weak var radioAuthorisze: RadioButton!
    var expireDate: String!
    var followingEvent: String!
    override func viewDidLoad() {
        super.viewDidLoad()

        loadValues()
        
        // Do any additional setup after loading the view...
    }

    @IBAction override func buttonBackAction() {
        saveValues()
        self.navigationController?.popViewController(animated: true)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func radioAction(_ sender: AnyObject) {
        
        if sender.tag == 1{
            self.expireDate = ""
            self.followingEvent = ""
        
        }else if sender.tag == 2{
            PopupTextField.popUpView().showInViewController(self, WithTitle: "EXPIRE DATE:", placeHolder: "PICK A DATE", textFormat: .dateInCurrentYear, completion: { (popUpView, textField) in
                if !textField.isEmpty{
                    self.expireDate = textField.text
                }else{
                    self.expireDate = ""
                }
                popUpView.removeFromSuperview()
            })
            self.followingEvent = ""
            
        }else if sender.tag == 3{
            PopupTextView.popUpView().showWithPlaceHolder("Type Details Here", completion: { (popUpView, textView) in
                if !textView.isEmpty{
                    
                    self.followingEvent = textView.text
                }else{
                    self.followingEvent = ""
                }
                popUpView.removeFromSuperview()
            })
            self.expireDate = ""
            
        }
        
    }
    
    func saveValues(){
        patient.radioAuthorize = radioAuthorisze.selected == nil ? 0 : radioAuthorisze.selected.tag
        patient.makeDisclosure = textFieldMakeDisclosure.text
        patient.receiveDisclosure = textFieldReceiveDisclosure.text
        patient.expireDate = self.expireDate
        patient.followingEvent = self.followingEvent
    }
    
    func loadValues(){
     
        radioAuthorisze.setSelectedWithTag(patient.radioAuthorize)
       textFieldMakeDisclosure.text =  patient.makeDisclosure
       textFieldReceiveDisclosure.text =  patient.receiveDisclosure
       self.expireDate =  patient.expireDate
       self.followingEvent =   patient.followingEvent
    }
    
    @IBAction func buttonNextAction() {
        self.view.endEditing(true)
      
            saveValues()
            let nitrous = consentStoryBoard2.instantiateViewController(withIdentifier: "kAuthorizVC5") as! AuthorizeVC5
            nitrous.patient = self.patient
            self.navigationController?.pushViewController(nitrous, animated: true)
        
    }
    
}
