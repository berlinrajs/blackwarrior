//
//  ApicoectomyVC.swift
//  SmileDesignDentalSpa
//
//  Created by APPLE on 12/5/16.
//  Copyright © 2016 srs. All rights reserved.
//

import UIKit

class AuthorizeVC6: MCViewController {

    @IBOutlet weak var sign: SignatureView!
    @IBOutlet weak var date: DateLabel!
    @IBOutlet weak var patientName: UILabel!
     @IBOutlet weak var textFieldAddress: MCTextField!
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        date.todayDate = patient.dateToday
        patientName.text = patient.fullName
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.....
    }
    
    @IBAction override func buttonBackAction() {
        saveValues()
        self.navigationController?.popViewController(animated: true)
    }
    
    func saveValues(){
        patient.authorizeAddress = textFieldAddress.text
        patient.signature3 = sign.signatureImage()
        
        
    }
    
    func loadValues(){
      textFieldAddress.text =  patient.authorizeAddress
        
    }

    @IBAction func doneAction(_ sender: AnyObject) {
        if !sign.isSigned(){
        self.showAlert("PLEASE SIGN THE FORM")
        }else if !date.dateTapped{
        self.showAlert("PLEASE SELECT THE DATE")
        }else{
            saveValues()
            let nextVc = self.storyboard?.instantiateViewController(withIdentifier: "kAuthorizeForm") as! AuthorizeForm
            nextVc.patient = self.patient
            self.navigationController?.pushViewController(nextVc, animated: true)
        
        }
        
    }


}
