//
//  PatientSignInStep2VC.swift
//  MConsentForms
//
//  Created by Berlin Raj on 22/09/16.
//  Copyright © 2016 SRS Web Solutions. All rights reserved.
//

import UIKit

class AuthorizationVC1: MCViewController {

    @IBOutlet weak var textFieldInitDate: MCTextField!
    @IBOutlet weak var textFieldClientName: MCTextField!
    @IBOutlet weak var textFieldRelationship: MCTextField!
    @IBOutlet weak var textFieldSocialSecurityNumber: MCTextField!
    @IBOutlet weak var radioIniated: RadioButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        textFieldSocialSecurityNumber.textFormat = .socialSecurity
        textFieldInitDate.textFormat = .dateInCurrentYear
        loadValues()
        
         if patient.authorizeClientOrProviderOptionTag == 1{
            textFieldClientName.alpha = 1.0
            textFieldClientName.isUserInteractionEnabled = true
            textFieldRelationship.text = ""
        
        }else if patient.authorizeClientOrProviderOptionTag == 2{
            textFieldClientName.alpha = 1.0
            textFieldClientName.isUserInteractionEnabled = true
            textFieldRelationship.alpha = 1.0
            textFieldRelationship.isUserInteractionEnabled = true
        
        }
        
        // Do any additional setup after loading the view
    }

    @IBAction override func buttonBackAction() {
        saveValues()
        self.navigationController?.popViewController(animated: true)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func radioAction(_ sender: AnyObject) {
        
        if sender.tag == 1{
            
            textFieldClientName.alpha = 1.0
            textFieldClientName.isUserInteractionEnabled = true
            textFieldRelationship.text = ""
            textFieldRelationship.alpha = 0.5
            textFieldRelationship.isUserInteractionEnabled = false
        }else{
            textFieldClientName.alpha = 1.0
            textFieldClientName.isUserInteractionEnabled = true
            textFieldRelationship.alpha = 1.0
            textFieldRelationship.isUserInteractionEnabled = true
        
        }
        
    }
    
    
    
    func saveValues(){
        patient.authorizeSS = textFieldSocialSecurityNumber.isEmpty ? "" : textFieldSocialSecurityNumber.text!
        patient.authorizeInitDate = textFieldInitDate.text
        patient.authorizeClientName = textFieldClientName.text
        patient.authorizeClientRelation = textFieldRelationship.text
        patient.authorizeClientOrProviderOptionTag = radioIniated.selected == nil ? 0 : radioIniated.selected.tag
    }
    
    func loadValues(){
        textFieldInitDate.text = patient.authorizeInitDate
        textFieldSocialSecurityNumber.text = patient.authorizeSS
        textFieldRelationship.text = patient.authorizeClientRelation
        textFieldClientName.text = patient.authorizeClientName
        radioIniated.setSelectedWithTag(patient.authorizeClientOrProviderOptionTag)

    }
    
    @IBAction func buttonNextAction() {
        self.view.endEditing(true)
         if !textFieldSocialSecurityNumber.isEmpty && !textFieldSocialSecurityNumber.text!.isSocialSecurityNumber {
            self.showAlert("PLEASE ENTER A VALID SOCIAL SECURITY NUMBER")
        } else if radioIniated.selected == nil {
            self.showAlert("PLEASE SELECT REQUIRED OPTION")
         }else if radioIniated.isSelected == false && textFieldRelationship.isEmpty{
         self.showAlert("PLEASE ENTER THE RELATIONSHIP")
         
         } else {
            saveValues()
            let nitrous = consentStoryBoard2.instantiateViewController(withIdentifier: "kAuthorizVC2") as! AuthorizeVC2
            nitrous.patient = self.patient
            self.navigationController?.pushViewController(nitrous, animated: true)
           
        }
    }
    
}
