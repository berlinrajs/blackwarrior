//
//  ApicoectomyForm.swift
//  SmileDesignDentalSpa
//
//  Created by APPLE on 12/5/16.
//  Copyright © 2016 srs. All rights reserved.
//

import UIKit

class AuthorizeForm: MCViewController {
    
    @IBOutlet weak var patientName: UILabel!
    @IBOutlet weak var labelDate: UILabel!
     @IBOutlet weak var labelDate1: UILabel!
    
    @IBOutlet weak var patientFullName: UILabel!
    @IBOutlet weak var patientDOb: UILabel!
    @IBOutlet weak var patientSSN: UILabel!
    @IBOutlet weak var dateAuthorizeInitiated: UILabel!
    @IBOutlet weak var ClientName: UILabel!
    @IBOutlet weak var ClientOrProviderRelation: UILabel!
    
    @IBOutlet weak var myDentalInfo: UILabel!
    @IBOutlet weak var mostRecYears: UILabel!
    @IBOutlet weak var dentalRecDates: UILabel!
    @IBOutlet weak var otherInfo: UILabel!
    
    @IBOutlet weak var authorizeFamilyMembers: UILabel!
    @IBOutlet weak var authorizeOtherInfo2: UILabel!
    
    @IBOutlet weak var makeDisclosure: UILabel!
    @IBOutlet weak var receiveDisclosure: UILabel!
    @IBOutlet weak var expireDate: UILabel!
    @IBOutlet weak var followingEvent: UILabel!

    @IBOutlet weak var relationToclientIfPersonalRep: UILabel!
    @IBOutlet weak var authorizAddress: UILabel!
    
    @IBOutlet var btnCollection1: [UIButton]!
    @IBOutlet var btnCollection2: [UIButton]!
    
    @IBOutlet weak var radio1: RadioButton!
    @IBOutlet weak var radio2: RadioButton!
    @IBOutlet weak var radio3: RadioButton!
    
    @IBOutlet weak var Sign1 : UIImageView!
    @IBOutlet weak var Sign2 : UIImageView!
    @IBOutlet weak var Sign3 : UIImageView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

       
        
        loadValues()
        // Do any additional setup after loading the view.
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    func loadValues(){
    patientFullName.text = patient.fullName
    patientDOb.text = patient.dateOfBirth
    patientSSN.text = patient.authorizeSS.socialSecurityNumber
    dateAuthorizeInitiated.text = patient.authorizeInitDate
    ClientName.text = patient.authorizeClientName
    ClientOrProviderRelation.text = patient.authorizeClientRelation
    
    myDentalInfo.text = patient.myDentalInfo
    mostRecYears.text = patient.mostRecYears
    dentalRecDates.text = patient.dentalRecDates
    otherInfo.text = patient.otherInfo
    
    authorizeFamilyMembers.text = patient.authorizFamilyMembers
    authorizeOtherInfo2.text = patient.authorizeOtherInfo2
        
    makeDisclosure.text = patient.makeDisclosure
    receiveDisclosure.text = patient.receiveDisclosure
    expireDate.text = patient.expireDate
    followingEvent.text = patient.followingEvent
    
    relationToclientIfPersonalRep.text = patient.relationshipToClientIfPersonalRep
    authorizAddress.text = patient.authorizeAddress
    
        
        for btn in btnCollection1 {
            btn.isSelected = patient.authorizeMultiArray1.contains(btn.tag)
        }
        
        
        for btn in btnCollection2 {
            btn.isSelected = patient.authorizeMultiArray2.contains(btn.tag)
        }
        
        radio1.setSelectedWithTag(patient.authorizeRadio1)
        radio2.setSelectedWithTag(patient.authorizeRadio2)
        radio3.setSelectedWithTag(patient.radioAuthorize)
        
         patientName.text = patient.fullName
        Sign1.image = patient.signature1
        Sign2.image = patient.signature2
        Sign3.image = patient.signature3
        labelDate.text = patient.dateToday
         labelDate1.text = patient.dateToday
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
