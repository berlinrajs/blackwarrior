//
//  AuthorizeVC2.swift
//  BlackWarrior
//
//  Created by APPLE on 12/18/16.
//  Copyright © 2016 Bala Murugan. All rights reserved.
//

import UIKit

class AuthorizeVC3: MCViewController {
    
    
    @IBOutlet var btnCollection: [UIButton]!

    var multiSelectArray: NSMutableArray! = NSMutableArray()
    
    var familyMembers: String!
    var otherInfo2: String!
   
    override func viewDidLoad() {
        super.viewDidLoad()
        loadValues()
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func MultiSelectButtonAction(wihSender sender: UIButton) {
        sender.isSelected = !sender.isSelected
        if multiSelectArray.contains(sender.tag) {
             if sender.tag == 5{
                self.familyMembers = ""
             }else if sender.tag == 6{
                self.otherInfo2 = ""
            }
            
            multiSelectArray.remove(sender.tag)
        } else {
            multiSelectArray.add(sender.tag)
             if sender.tag == 5{
                PopupTextField.popUpView().showInViewController(self, WithTitle: "FAMILY MEMBERS:", placeHolder: "TYPE HERE", textFormat: .default, completion: { (popUpView, textField) in
                    if !textField.isEmpty{
                        self.familyMembers = textField.text
                    }else{
                        self.familyMembers = ""
                    }
                    popUpView.removeFromSuperview()
                })
             }else if sender.tag == 6{
                PopupTextView.popUpView().showWithPlaceHolder("Type Details Here", completion: { (popUpView, textView) in
                    if !textView.isEmpty{
                        self.otherInfo2 = textView.text
                    }else{
                    self.otherInfo2 = ""
                    }
                    popUpView.removeFromSuperview()
                })
            }
        }
        
    }
    
    
    func saveValues(){
        patient.authorizeMultiArray2 = self.multiSelectArray
       patient.authorizFamilyMembers = self.familyMembers
        patient.authorizeOtherInfo2 = self.otherInfo2
    }
    
    func loadValues(){
    
        if patient.authorizeMultiArray2 != nil {
            self.multiSelectArray = patient.authorizeMultiArray2
            for btn in btnCollection {
                btn.isSelected = self.multiSelectArray.contains(btn.tag)
                
            }
        }
        self.familyMembers = patient.authorizFamilyMembers
        self.otherInfo2 = patient.authorizeOtherInfo2
    }
    
    @IBAction override func buttonBackAction() {
      saveValues()
        self.navigationController?.popViewController(animated: true)
    }

    
    @IBAction func btnAction(_ sender: AnyObject) {
        
        saveValues()
        let nitrous = consentStoryBoard2.instantiateViewController(withIdentifier: "kAuthorizVC4") as! AuthorizationVC4
        nitrous.patient = self.patient
        self.navigationController?.pushViewController(nitrous, animated: true)
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
