//
//  AuthorizeVC2.swift
//  BlackWarrior
//
//  Created by APPLE on 12/18/16.
//  Copyright © 2016 Bala Murugan. All rights reserved.
//

import UIKit

class AuthorizeVC2: MCViewController {
    
    
    @IBOutlet var btnCollection: [UIButton]!

    @IBOutlet weak var extraView: UIView!
    @IBOutlet weak var radio1: RadioButton!
    @IBOutlet weak var radio2: RadioButton!
    
    var multiSelectArray: NSMutableArray! = NSMutableArray()
    
    var myDentalInfo: String!
    var mostRecYears: String!
    var dentalRecDates: String!
    var otherInfo: String!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        loadValues()
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func MultiSelectButtonAction(wihSender sender: UIButton) {
        sender.isSelected = !sender.isSelected
        if multiSelectArray.contains(sender.tag) {
            
            if sender.tag == 1{
                self.myDentalInfo = ""
            }else if sender.tag == 2{
                self.mostRecYears = ""
            }else if sender.tag == 3{
                self.dentalRecDates = ""
            }else if sender.tag == 4{
                self.extraView.isUserInteractionEnabled = false
                self.extraView.alpha = 0.5
                radio1.deselectAllButtons()
                radio2.deselectAllButtons()
            }else if sender.tag == 5{
                self.otherInfo = ""
            }
            
            multiSelectArray.remove(sender.tag)
        } else {
            multiSelectArray.add(sender.tag)
            if sender.tag == 1{
                PopupTextField.popUpView().showInViewController(self, WithTitle: "IF SO,", placeHolder: "SPECIFY HERE", textFormat: .default, completion: { (popUpView, textField) in
                    if !textField.isEmpty{
                    self.myDentalInfo = textField.text
                    }else{
                    self.myDentalInfo = ""
                    }
                    popUpView.removeFromSuperview()
                })
            
            }else if sender.tag == 2{
                PopupTextField.popUpView().showInViewController(self, WithTitle: "IF SO,", placeHolder: "YEARS", textFormat: .alphaNumeric, completion: { (popUpView, textField) in
                    if !textField.isEmpty{
                        self.mostRecYears = textField.text
                    }else{
                        self.mostRecYears = ""
                    }
                    popUpView.removeFromSuperview()
                })
            }else if sender.tag == 3{
                PopupTextField.popUpView().showInViewController(self, WithTitle: "IF SO,", placeHolder: "DATES", textFormat: .alphaNumeric, completion: { (popUpView, textField) in
                    if !textField.isEmpty{
                        self.dentalRecDates = textField.text
                    }else{
                        self.dentalRecDates = ""
                    }
                    popUpView.removeFromSuperview()
                })
            }else if sender.tag == 4 {
                self.extraView.isUserInteractionEnabled = true
                self.extraView.alpha = 1.0
            
            } else if sender.tag == 5{
                PopupTextField.popUpView().showInViewController(self, WithTitle: "IF SO,", placeHolder: "DESCRIBE", textFormat: .default, completion: { (popUpView, textField) in
                    if !textField.isEmpty{
                        self.otherInfo = textField.text
                    }else{
                        self.otherInfo = ""
                    }
                    popUpView.removeFromSuperview()
                })
            }
        }
        
    }
    
    
    func saveValues(){
        patient.authorizeMultiArray1 = self.multiSelectArray
        patient.authorizeRadio1 = radio1.selected == nil ? 0: radio1.selected.tag
        patient.authorizeRadio2 = radio2.selected == nil ? 0: radio2.selected.tag
        patient.myDentalInfo = self.myDentalInfo
        patient.mostRecYears = self.mostRecYears
        patient.dentalRecDates = self.dentalRecDates
        patient.otherInfo = self.otherInfo
    }
    
    func loadValues(){
    
        if patient.authorizeMultiArray1 != nil {
            self.multiSelectArray = patient.authorizeMultiArray1
            for btn in btnCollection {
                btn.isSelected = self.multiSelectArray.contains(btn.tag)
                if self.multiSelectArray.contains(4){
                    self.extraView.isUserInteractionEnabled = true
                    self.extraView.alpha = 1.0
                }
            }
        }
        radio1.setSelectedWithTag(patient.authorizeRadio1)
        radio2.setSelectedWithTag(patient.authorizeRadio2)
        self.myDentalInfo = patient.myDentalInfo
         self.mostRecYears = patient.mostRecYears
        self.dentalRecDates = patient.dentalRecDates
        self.otherInfo =  patient.otherInfo
    }
    
    @IBAction override func buttonBackAction() {
      saveValues()
        self.navigationController?.popViewController(animated: true)
    }

    
    @IBAction func btnAction(_ sender: AnyObject) {
        
        saveValues()
        let nitrous = consentStoryBoard2.instantiateViewController(withIdentifier: "kAuthorizVC3") as! AuthorizeVC3
        nitrous.patient = self.patient
        self.navigationController?.pushViewController(nitrous, animated: true)
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
