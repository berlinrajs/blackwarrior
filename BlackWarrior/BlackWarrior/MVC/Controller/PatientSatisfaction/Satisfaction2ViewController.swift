//
//  Satisfaction2ViewController.swift
//  BlackWarrior
//
//  Created by Bala Murugan on 12/19/16.
//  Copyright © 2016 Bala Murugan. All rights reserved.
//

import UIKit

class Satisfaction2ViewController: MCViewController {

    @IBOutlet var tableViewQuestions: UITableView!
    @IBOutlet var buttonVerified: UIButton!
    @IBOutlet weak var textviewComments : MCTextView!
    @IBOutlet weak var labelHeading : UILabel!
    var arrayHeading = ["APPOINTMENTS","FACILITIES","STAFF","TREATMENT"]
    var index : Int = 0

    override func viewDidLoad() {
        super.viewDidLoad()
        loadValue()

    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func loadValue()  {
        labelHeading.text = arrayHeading[index]
        textviewComments.placeholder = "PLEASE TYPE HERE"
        textviewComments.placeholderColor = UIColor.lightGray
        textviewComments.text = patient.value(forKeyPath: "satisfaction.comments" + "\(index + 1)") as! String == "" ? "PLEASE TYPE HERE" : patient.value(forKeyPath: "satisfaction.comments" + "\(index + 1)") as! String
        textviewComments.textColor = patient.value(forKeyPath: "satisfaction.comments" + "\(index + 1)") as! String == "" ? UIColor.lightGray : UIColor.black
        
    }
    
    func saveValue()  {
        let comments = textviewComments.isEmpty ? "" : textviewComments.text!
        patient.setValue(comments, forKeyPath: "satisfaction.comments" + "\(index + 1)")
    }
    
    @IBAction override func buttonBackAction() {
        saveValue()
        super.buttonBackAction()
    }

    @IBAction func buttonVerifiedAction(withSender sender: UIButton) {
        sender.isSelected = !sender.isSelected
    }

    @IBAction func onNextButtonPressed (withSender sender : UIButton){
        if buttonVerified.isSelected == false {
            self.showAlert("PLEASE CONFIRM THAT YOU HAVE ANSWERED ABOVE QUESTIONS")
        } else {
            saveValue()
            if index == 3{
                let satisfaction = consentStoryBoard.instantiateViewController(withIdentifier: "Satisfaction3VC") as! Satisfaction3ViewController
                satisfaction.patient = self.patient
                self.navigationController?.pushViewController(satisfaction, animated: true)
            }else{
            let satisfaction = consentStoryBoard.instantiateViewController(withIdentifier: "Satisfaction2VC") as! Satisfaction2ViewController
            satisfaction.patient = self.patient
            satisfaction.index = index + 1
            self.navigationController?.pushViewController(satisfaction, animated: true)
            }
            }
        }
    
}

extension Satisfaction2ViewController : UITableViewDataSource,UITableViewDelegate {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return patient.satisfaction.surveyQuestions[index].count
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return index == 3 ? 50 : 60
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath) as! PatientInfoCell
        cell.tag = indexPath.row
        cell.configCell(patient.satisfaction.surveyQuestions[index][indexPath.row])
        
        return cell
    }
}

