//
//  SatisfactionFormViewController.swift
//  BlackWarrior
//
//  Created by Bala Murugan on 12/19/16.
//  Copyright © 2016 Bala Murugan. All rights reserved.
//

import UIKit

class SatisfactionFormViewController: MCViewController {

    @IBOutlet weak var radioButtonAge : RadioButton!
    @IBOutlet weak var radioButtonGender : RadioButton!
    @IBOutlet weak var radioButtonVisits : RadioButton!
    @IBOutlet weak var radioButtonTreatment1 : RadioButton!
    @IBOutlet weak var radioButtonTreatment2 : RadioButton!
    @IBOutlet      var radioButtonAppointment : [RadioButton]!
    @IBOutlet weak var labelAppointmentComments : UILabel!
    @IBOutlet      var radioButtonFacilities : [RadioButton]!
    @IBOutlet weak var labelFacilityComments : UILabel!
    
    @IBOutlet      var radioButtonStaff : [RadioButton]!
    @IBOutlet weak var labelStaffComment : UILabel!
    @IBOutlet      var radioButtonTreatment : [RadioButton]!
    @IBOutlet weak var labelTreatmentComment : UILabel!
    @IBOutlet weak var labelBestOffice : UILabel!
    @IBOutlet weak var labelLeastOffice : UILabel!
    @IBOutlet weak var labelExperience : UILabel!
    @IBOutlet weak var labelPatientName : UILabel!
    @IBOutlet weak var signaturePatient : UIImageView!
    @IBOutlet weak var labelDate : UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        loadValue()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func loadValue()  {
        radioButtonAge.setSelectedWithTag(patient.satisfaction.ageTag)
        radioButtonGender.setSelectedWithTag(patient.satisfaction.genderTag)
        radioButtonVisits.setSelectedWithTag(patient.satisfaction.visitTag)
        radioButtonTreatment1.setSelectedWithTag(patient.satisfaction.treatmentTag1)
        radioButtonTreatment2.setSelectedWithTag(patient.satisfaction.treatmentTag2)
        (radioButtonAppointment as NSArray).enumerateObjects({ (btn, idx, stop) in
            let quest : MCQuestion = patient.satisfaction.surveyQuestions[0][idx]
            (btn as AnyObject).setSelectedWithTag(quest.selectedIndex)
            
        })
        labelAppointmentComments.text = patient.satisfaction.comments1
        (radioButtonFacilities as NSArray).enumerateObjects({ (btn, idx, stop) in
            let quest : MCQuestion = patient.satisfaction.surveyQuestions[1][idx]
            (btn as AnyObject).setSelectedWithTag(quest.selectedIndex)
            
        })
        labelFacilityComments.text = patient.satisfaction.comments2
        
        (radioButtonStaff as NSArray).enumerateObjects({ (btn, idx, stop) in
            let quest : MCQuestion = patient.satisfaction.surveyQuestions[2][idx]
            (btn as AnyObject).setSelectedWithTag(quest.selectedIndex)
            
        })
        labelStaffComment.text = patient.satisfaction.comments3
        (radioButtonTreatment as NSArray).enumerateObjects({ (btn, idx, stop) in
            let quest : MCQuestion = patient.satisfaction.surveyQuestions[3][idx]
            (btn as AnyObject).setSelectedWithTag(quest.selectedIndex)
            
        })
        labelTreatmentComment.text = patient.satisfaction.comments4
        labelBestOffice.text = patient.satisfaction.bestOffice
        labelLeastOffice.text = patient.satisfaction.leastOffice
        labelExperience.text = patient.satisfaction.experience
        labelPatientName.text = patient.fullName
        signaturePatient.image = patient.satisfaction.signPatient
        labelDate.text = patient.dateToday
    }


}
