//
//  Satisfaction3ViewController.swift
//  BlackWarrior
//
//  Created by Bala Murugan on 12/19/16.
//  Copyright © 2016 Bala Murugan. All rights reserved.
//

import UIKit

class Satisfaction3ViewController: MCViewController {
    
    @IBOutlet weak var textviewBest : MCTextView!
    @IBOutlet weak var textviewLeast : MCTextView!
    @IBOutlet weak var textviewExperience : MCTextView!
    @IBOutlet weak var signaturePatient : SignatureView!
    @IBOutlet weak var labelDate : DateLabel!
    @IBOutlet weak var labelPatientName : UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        labelPatientName.text = patient.fullName
        loadValue()
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func loadValue()  {
        labelDate.todayDate = patient.dateToday
        textviewBest.placeholder = "PLEASE TYPE HERE"
        textviewBest.placeholderColor = UIColor.lightGray
        textviewBest.text = patient.satisfaction.bestOffice == "" ? "PLEASE TYPE HERE" : patient.satisfaction.bestOffice
        textviewBest.textColor = patient.satisfaction.bestOffice == "" ? UIColor.lightGray : UIColor.black
        
        textviewLeast.placeholder = "PLEASE TYPE HERE"
        textviewLeast.placeholderColor = UIColor.lightGray
        textviewLeast.text = patient.satisfaction.leastOffice == "" ? "PLEASE TYPE HERE" : patient.satisfaction.leastOffice
        textviewLeast.textColor = patient.satisfaction.leastOffice == "" ? UIColor.lightGray : UIColor.black

        
        textviewExperience.placeholder = "PLEASE TYPE HERE"
        textviewExperience.placeholderColor = UIColor.lightGray
        textviewExperience.text = patient.satisfaction.experience == "" ? "PLEASE TYPE HERE" : patient.satisfaction.experience
        textviewExperience.textColor = patient.satisfaction.experience == "" ? UIColor.lightGray : UIColor.black

        
    }
    
    func saveValue()  {
        patient.satisfaction.bestOffice = textviewBest.isEmpty ? "" : textviewBest.text!
        patient.satisfaction.leastOffice = textviewLeast.isEmpty ? "" : textviewLeast.text!
        patient.satisfaction.experience = textviewExperience.isEmpty ? "" : textviewExperience.text!
    }
    
    @IBAction override func buttonBackAction() {
        saveValue()
        super.buttonBackAction()
    }
    
    @IBAction func onNextButtonPressed (withSender sender : UIButton){
        if !signaturePatient.isSigned() {
            self.showAlert("PLEASE SIGN THE FORM")
        }else if !labelDate.dateTapped{
            self.showAlert("PLEASE SELECT THE DATE")
        } else {
            saveValue()
            patient.satisfaction.signPatient = signaturePatient.signatureImage()
           let satisfaction = consentStoryBoard.instantiateViewController(withIdentifier: "SatisfactionFormVC") as! SatisfactionFormViewController
           satisfaction.patient = self.patient
           self.navigationController?.pushViewController(satisfaction, animated: true)
            
        }
    }
    
}


