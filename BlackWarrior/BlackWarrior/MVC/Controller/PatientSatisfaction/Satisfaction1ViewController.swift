//
//  Satisfaction1ViewController.swift
//  BlackWarrior
//
//  Created by Bala Murugan on 12/18/16.
//  Copyright © 2016 Bala Murugan. All rights reserved.
//

import UIKit

class Satisfaction1ViewController: MCViewController {

    @IBOutlet weak var dropdownAge : BRDropDown!
    @IBOutlet weak var dropdownGender : BRDropDown!
    @IBOutlet weak var dropdownVisits : BRDropDown!
    @IBOutlet weak var radioButton1 : RadioButton!
    @IBOutlet weak var radioButton2 : RadioButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        dropdownAge.items = ["18-25","26-40","41-55","Over 55"]
        dropdownAge.placeholder = "-- SELECT --"
        dropdownGender.items = ["MALE","FEMALE"]
        dropdownGender.placeholder = "-- SELECT --"
        dropdownVisits.items = ["1","2","3","4","5 or more"]
        dropdownVisits.placeholder = "-- SELECT --"

        loadValue()
        

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func loadValue()  {
        dropdownAge.selectedIndex = patient.satisfaction.ageTag
        dropdownGender.selectedIndex = patient.satisfaction.genderTag
        dropdownVisits.selectedIndex = patient.satisfaction.visitTag
        radioButton1.setSelectedWithTag(patient.satisfaction.treatmentTag1)
        radioButton2.setSelectedWithTag(patient.satisfaction.treatmentTag2)
    }
    
    func saveValue() {
        patient.satisfaction.ageTag = dropdownAge.selectedOption == nil ? 0 : dropdownAge.selectedIndex
        patient.satisfaction.genderTag = dropdownGender.selectedOption == nil ? 0 : dropdownGender.selectedIndex
        patient.satisfaction.visitTag = dropdownVisits.selectedOption == nil ? 0 : dropdownVisits.selectedIndex
        patient.satisfaction.treatmentTag1 = radioButton1.selected == nil ? 0 : radioButton1.selected.tag
        patient.satisfaction.treatmentTag2 = radioButton2.selected == nil ? 0 : radioButton2.selected.tag
    }
    
    @IBAction override func buttonBackAction() {
        saveValue()
        super.buttonBackAction()
    }

    
    @IBAction func onNextButtonPressed (withSender sender : UIButton){
        if dropdownAge.selectedOption == nil || dropdownGender.selectedOption == nil || dropdownVisits.selectedOption == nil{
            self.showAlert("PLEASE ENTER ALL THE REQUIRED FIELDS")
        }else if (radioButton1.selected == nil && radioButton2.selected != nil) || (radioButton1.selected != nil && radioButton2.selected == nil){
            self.showAlert("PLEASE COMPLETE THE TREATMENT DETAILS")
        }else{
            saveValue()
            let satisfaction = consentStoryBoard.instantiateViewController(withIdentifier: "Satisfaction2VC") as! Satisfaction2ViewController
            satisfaction.patient = self.patient
            self.navigationController?.pushViewController(satisfaction, animated: true)
        }
    }


}
