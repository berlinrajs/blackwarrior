//
//  MCViewController.swift
//  WestgateSmiles
//
//  Created by Leojin Bose on 2/25/16.
//  Copyright © 2016 SRS. All rights reserved.
//

import UIKit
// FIXME: comparison operators with optionals were removed from the Swift Standard Libary.
// Consider refactoring the code to use the non-optional operators.
fileprivate func < <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
  switch (lhs, rhs) {
  case let (l?, r?):
    return l < r
  case (nil, _?):
    return true
  default:
    return false
  }
}

// FIXME: comparison operators with optionals were removed from the Swift Standard Libary.
// Consider refactoring the code to use the non-optional operators.
fileprivate func > <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
  switch (lhs, rhs) {
  case let (l?, r?):
    return l > r
  default:
    return rhs < lhs
  }
}


let mainStoryBoard = UIStoryboard(name: "Main", bundle: Bundle.main)
let medicalStoryboard = UIStoryboard(name: "MedicalHistory", bundle: Bundle.main)
let newPatientStoryboard = UIStoryboard(name: "NewPatient", bundle: Bundle.main)
let consentStoryBoard = UIStoryboard(name: "Consent", bundle: Bundle.main)
let consentStoryBoard2 = UIStoryboard(name: "Consent2", bundle: Bundle.main)
let serverPath = "https://opcommunity.mncell.com/formreviewapp/"
class MCViewController: UIViewController {
    
    @IBOutlet var buttonSubmit: MCButton?
    @IBOutlet var buttonBack: MCButton?
    
    @IBOutlet var pdfView: UIScrollView?
    
    var patient: MCPatient!
     var completion: ((_ success: Bool) -> Void)?

    override func viewDidLoad() {
        super.viewDidLoad()
        edgesForExtendedLayout = UIRectEdge()
        
        configureNavigationButtons()
        // Do any additional setup after loading the view.
    }
    
    func configureNavigationButtons() {
        self.buttonSubmit?.backgroundColor = UIColor.green
        self.buttonBack?.isHidden = self.buttonSubmit == nil && isFromPreviousForm
        
        if self.buttonBack != nil && self.buttonSubmit == nil && isFromPreviousForm {
            self.navigationController?.viewControllers.removeSubrange(1...self.navigationController!.viewControllers.index(of: self)! - 1)
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    var isFromPreviousForm: Bool {
        get {
            return navigationController?.viewControllers.count > 3 ? true : false
        }
    }
    
    @IBAction func buttonBackAction() {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func onSubmitButtonPressed() {
        self.buttonSubmit?.isUserInteractionEnabled = false
        self.buttonBack?.isUserInteractionEnabled = false
        if !Reachability.isConnectedToNetwork() {
            self.showAlert("Your device is not connected to internet. Please go to settings to connect.", buttonTitles: ["Settings", "Cancel"], completion: { (buttonIndex) in
                if buttonIndex == 0 {
                    let settingsUrl = NSURL(string: UIApplicationOpenSettingsURLString)
                    if let url = settingsUrl {
                        UIApplication.shared.openURL(url as URL)
                    }
                } else {
                    
                }
            })
            self.buttonSubmit?.isUserInteractionEnabled = true
            self.buttonBack?.isUserInteractionEnabled = true
            return
        }
        uploadPdfToDrive()
    }
    
    func sendFormToGoogleDrive(completion: @escaping (_ success: Bool) -> Void) {
        self.uploadPdfToDrive()
    }
    
    func sendFormToServer() {
        if !Reachability.isConnectedToNetwork() {
            self.showAlert("Your device is not connected to internet. Please go to settings to connect.", buttonTitles: ["Settings", "Cancel"], completion: { (buttonIndex) in
                if buttonIndex == 1 {
                    let settingsUrl = NSURL(string: UIApplicationOpenSettingsURLString)
                    if let url = settingsUrl {
                        UIApplication.shared.openURL(url as URL)
                    }
                } else {
                    
                }
            })
            return
        }
        var image: UIImage?
        self.buttonSubmit?.isHidden = true
        self.buttonBack?.isHidden = true
        self.pdfView?.isScrollEnabled = false
        self.pdfView?.clipsToBounds = false
        let size: CGSize = CGSize(width: self.pdfView!.contentSize.width, height: self.pdfView!.contentSize.height)
        
        let savedContentOffset = self.pdfView!.contentOffset
        UIGraphicsBeginImageContext(size)
        self.pdfView?.layer.render(in: UIGraphicsGetCurrentContext()!)
        
        image = UIGraphicsGetImageFromCurrentImageContext()
        self.pdfView?.contentOffset = savedContentOffset
        UIGraphicsEndImageContext()
        
        func resetValues() {
            self.buttonSubmit?.isHidden = false
            self.buttonBack?.isHidden = false
            self.pdfView?.isScrollEnabled = true
            self.pdfView?.clipsToBounds = true
            BRProgressHUD.hide()
        }
        
        
        if let imageData = image {
            BRProgressHUD.show()
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "MM'_'dd'_'yyyy"
            let dateString = dateFormatter.string(from: Date()).uppercased()
            dateFormatter.dateFormat = "MM'-'dd'-'yyyy"
            let manager = AFHTTPSessionManager(baseURL: URL(string: serverPath))
            manager.responseSerializer.acceptableContentTypes = ["text/html"]
            //http://opcommunity.mncell.com/formreviewapp/upload_medical_history_form.php?first_name=&last_name=&doc_date=&form_name=&client_id=&file_name=
            manager.post("upload_medical_history_form.php", parameters: ["first_name": patient.firstName, "last_name": patient.lastName, "doc_date": dateFormatter.string(from: Date()), "form_name": self.patient.selectedForms.first!.formTitle.fileName, "client_id": kAppKey, "file_name": self.patient.selectedForms.first!.formTitle.fileName], constructingBodyWith: { (data) in
                dateFormatter.dateFormat = "MMddyyyyHHmmSSS"
                let randomString = dateFormatter.string(from: Date()).uppercased()
                let fileName = "\(self.patient.fullName.fileName)_\(dateString)_\(self.patient.selectedForms.first!.formTitle.fileName)_\(randomString).jpg"
                data.appendPart(withFileData: UIImageJPEGRepresentation(imageData, 1.0)!, name: self.patient.selectedForms.first!.formTitle.fileName, fileName: fileName, mimeType: "image/jpeg")
            }, progress: { (progress) in
                
            }, success: { (task, result) in
                resetValues()
                self.patient.selectedForms.removeFirst()
                self.gotoNextForm()
            }) { (task, error) in
                resetValues()
                self.showAlert(error.localizedDescription)
            }
        } else {
            self.showAlert("SOMETHIG WENT WRONG\nPLEASE TRY AGAIN")
            resetValues()
        }
    }
    
    func uploadPdfToDrive (){
        let pdfManager = PDFManager.sharedInstance()
        pdfManager.authorizeDrive(self) { (success) -> Void in
            if success {
                self.buttonSubmit?.isHidden = true
                self.buttonBack?.isHidden = true

                pdfManager.uploadToGoogleDrive(self.pdfView == nil ? self.view : self.pdfView!, patient: self.patient, completionBlock: { (finished) -> Void in
                    if finished {
                        if let _ = self.completion {
                            self.completion!(true)
                        } else {
                            self.patient.selectedForms.removeFirst()
                            self.gotoNextForm()
                        }
                    } else {
                        self.buttonSubmit?.isHidden = false
                        self.buttonBack?.isHidden = false
                        self.buttonSubmit?.isUserInteractionEnabled = true
                        self.buttonBack?.isUserInteractionEnabled = true
                    }
                })
            } else {
                self.buttonSubmit?.isHidden = false
                self.buttonBack?.isHidden = false
                self.buttonSubmit?.isUserInteractionEnabled = true
                self.buttonBack?.isUserInteractionEnabled = true
            }
        }
    }
    
    
    
    func gotoNextForm() {
        let formNames = (patient.selectedForms as NSArray).value(forKey: "formTitle") as! [String]
        
        if formNames.contains(kNewPatientSignInForm) || formNames.contains(kPatientSignInForm) {
            let patientSignIn = newPatientStoryboard.instantiateViewController(withIdentifier: "kPatientRegistrationStep1VC") as! PatientRegistrationStep1ViewController
            patientSignIn.patient = self.patient
            self.navigationController?.pushViewController(patientSignIn, animated: true)
        }else if formNames.contains(kMedicalHistory) {
            let medicalHis = medicalStoryboard.instantiateViewController(withIdentifier: "kMedicalHistoryStep1ViewController") as! MedicalHistoryStep1ViewController
            medicalHis.patient = self.patient
            self.navigationController?.pushViewController(medicalHis, animated: true)

        } else if formNames.contains(kInsuranceCard) {
            let insuranceCard = mainStoryBoard.instantiateViewController(withIdentifier: "kCardImageCaptureVC") as! CardImageCaptureVC
            insuranceCard.patient = self.patient
            self.navigationController?.pushViewController(insuranceCard, animated: true)
        } else if formNames.contains(kDrivingLicense) {
            let drivingLicense = mainStoryBoard.instantiateViewController(withIdentifier: "kCardImageCaptureVC") as! CardImageCaptureVC
            drivingLicense.patient = self.patient
            drivingLicense.isDrivingLicense = true
            self.navigationController?.pushViewController(drivingLicense, animated: true)
        }else if formNames.contains(kSelfieForm) {
            let drivingLicense = newPatientStoryboard.instantiateViewController(withIdentifier: "kSelfieStep1VC") as! SelfieStep1VC
            drivingLicense.patient = self.patient
            self.navigationController?.pushViewController(drivingLicense, animated: true)
            
        } else if formNames.contains(kFinancialAgreement) {
            let finance = consentStoryBoard.instantiateViewController(withIdentifier: "Finance1VC") as! FinancialAgreement1ViewController
            finance.patient = self.patient
            self.navigationController?.pushViewController(finance, animated: true)
        }else if formNames.contains(kAppointment){
            let appointment = consentStoryBoard.instantiateViewController(withIdentifier: "Appointment1VC") as! Appointment1ViewController
            appointment.patient = self.patient
            self.navigationController?.pushViewController(appointment, animated: true)
        }else if formNames.contains(kAuthorization){
            let periodontal = consentStoryBoard2.instantiateViewController(withIdentifier: "kAuthorizVC1") as! AuthorizationVC1
            periodontal.patient = self.patient
            self.navigationController?.pushViewController(periodontal, animated: true)
        }else if formNames.contains(kImmediateDenture){
            let immediate = consentStoryBoard.instantiateViewController(withIdentifier: "ImmediateDenture1VC") as! ImmediateDenture1ViewController
            immediate.patient = self.patient
            self.navigationController?.pushViewController(immediate, animated: true)
        }else if formNames.contains(kArtificialDenture){
            let artificial = consentStoryBoard.instantiateViewController(withIdentifier: "ArtificialDenture1VC") as! ArtificialDentures1ViewController
            artificial.patient = self.patient
            self.navigationController?.pushViewController(artificial, animated: true)
        }else if formNames.contains(kNitrousOxide){
            let nitrous = consentStoryBoard.instantiateViewController(withIdentifier: "NitrousOxide1VC") as! NitrousOxide1ViewController
            nitrous.patient = self.patient
            self.navigationController?.pushViewController(nitrous, animated: true)
        }else if formNames.contains(kPeriodontal){
            let periodontal = consentStoryBoard.instantiateViewController(withIdentifier: "Periodontal1VC") as! Periodontal1ViewController
            periodontal.patient = self.patient
            self.navigationController?.pushViewController(periodontal, animated: true)
        }else if formNames.contains(kEndodontic){
            let endodontic = consentStoryBoard.instantiateViewController(withIdentifier: "Endodontic1VC") as! Endodontic1ViewController
            endodontic.patient = self.patient
            self.navigationController?.pushViewController(endodontic, animated: true)
        }else if formNames.contains(kLiability){
            let liability = consentStoryBoard.instantiateViewController(withIdentifier: "Liability1VC") as! Liability1ViewController
            liability.patient = self.patient
            self.navigationController?.pushViewController(liability, animated: true)
        }else if formNames.contains(kSchoolExcuse){
            let school = consentStoryBoard.instantiateViewController(withIdentifier: "SchoolExcuse1VC") as! SchoolExcuse1ViewController
            school.patient = self.patient
            self.navigationController?.pushViewController(school, animated: true)
        }else if formNames.contains(kMedicalRelease){
            let nitrous = consentStoryBoard2.instantiateViewController(withIdentifier: "kMedReleaseVC1") as! MedReleaseVC1
            nitrous.patient = self.patient
            self.navigationController?.pushViewController(nitrous, animated: true)
        }else if formNames.contains(kOralSurgery){
            let oral = consentStoryBoard.instantiateViewController(withIdentifier: "OralSurgery1VC") as! OralSurgery1ViewController
            oral.patient = self.patient
            self.navigationController?.pushViewController(oral, animated: true)
        }else if formNames.contains(kReferral){
            let referral = consentStoryBoard.instantiateViewController(withIdentifier: "Referral1VC") as! Referral1ViewController
            referral.patient = self.patient
            self.navigationController?.pushViewController(referral, animated: true)
        }else if formNames.contains(kSatisfaction){
            let satisfaction = consentStoryBoard.instantiateViewController(withIdentifier: "Satisfaction1VC") as! Satisfaction1ViewController
            satisfaction.patient = self.patient
            self.navigationController?.pushViewController(satisfaction, animated: true)

        } else {
            NotificationCenter.default.post(name: Notification.Name(rawValue: kFormsCompletedNotification), object: nil)
            self.navigationController?.popToRootViewController(animated: true)
        }
    }
}
