//
//  MedicalHistoryStep2ViewController.swift
//  ABC Clinic
//
//  Created by Leojin Bose on 2/26/16.
//  Copyright © 2016 SRS. All rights reserved.
//

import UIKit

class MedicalHistoryStep2ViewController: MCViewController {
    
    @IBOutlet weak var radioButton: RadioButton!
    @IBOutlet weak var tableViewQuestions: UITableView!
    @IBOutlet weak var viewTableBG: MCView!
    
    @IBOutlet weak var radioButtonAlergetic: RadioButton!
    @IBOutlet weak var tableViewAlergetic: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        viewTableBG.borderColor = UIColor.white.withAlphaComponent(0.5)
        if patient.isWomen == nil {
            radioButton.isSelected = false
        } else {
            radioButton.isSelected = patient.isWomen!
        }
        if patient.controlledSubstancesClicked == nil {
            radioButtonAlergetic.deselectAllButtons()
        } else {
            radioButtonAlergetic.isSelected = patient.controlledSubstancesClicked
        }
        
        self.fetchData()
    }
    
    func fetchData() {
        if self.patient.medicalHistoryQuestions2 == nil {
            self.patient.medicalHistoryQuestions2 = MCQuestion.getArrayOfQuestions(["Pregnant / trying to get pregnant", "Nursing", "Taking oral contraceptives", "None"])
        }
        self.tableViewQuestions.reloadData()
        
        if patient.medicalHistoryQuestions3 == nil {
            self.patient.medicalHistoryQuestions3 = MCQuestion.getArrayOfQuestions(["Aspirin", "Penicillin", "Codeine", "Acrylic", "Metal", "Latex", "Sulfa drugs", "Local anesthetics", "Others"])
        }
        self.tableViewAlergetic.reloadData()
    }
    
    func findEmptyValue() -> MCQuestion? {
        for question in self.patient.medicalHistoryQuestions2 {
            if question.selectedOption == true {
                return question
            }
        }
        return nil
    }
    
    @IBAction func buttonActionNext(_ sender: AnyObject) {
        func gotoNextView() {
            if radioButtonAlergetic.selected == nil {
                self.showAlert("PLEASE ANSWER ALL QUESTIONS")
            } else {
                patient.medicalHistoryQuestions2 = self.patient.medicalHistoryQuestions2
                patient.medicalHistoryQuestions3 = self.patient.medicalHistoryQuestions3
                let medicalHistoryStep4VC = medicalStoryboard.instantiateViewController(withIdentifier: "kMedicalHistoryStep4ViewController") as! MedicalHistoryStep4ViewController
                medicalHistoryStep4VC.patient = patient
                self.navigationController?.pushViewController(medicalHistoryStep4VC, animated: true)
            }
        }
        if radioButton.selected != nil && radioButton.selected.tag == 1 {
            if let _ = findEmptyValue() {
                gotoNextView()
            } else {
                self.showAlert("IF YOU ARE WOMAN PLEASE SELECT ANY OF THE FOLLOWING")
            }
        } else if radioButton.selected == nil {
            self.showAlert("PLEASE SELECT ARE YOU WOMEN OR NOT")
        } else {
            gotoNextView()
        }
    }
    
    @IBAction func buttonActionWomen(_ sender: RadioButton) {
        if sender.tag == 2 {
            for obj in patient.medicalHistoryQuestions2 {
                obj.selectedOption = false
            }
            viewTableBG.borderColor = UIColor.white.withAlphaComponent(0.5)
        } else {
            viewTableBG.borderColor = UIColor.white
        }
        patient.isWomen = radioButton.isSelected
        tableViewQuestions.reloadData()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    @IBAction func radioButtonAction(_ sender: RadioButton) {
        patient.controlledSubstancesClicked = radioButtonAlergetic.isSelected
        if sender.tag == 1 {
            PopupTextView.popUpView().showWithPlaceHolder("IF YES TYPE HERE", completion: { (popUpView, textView) in
                if !textView.isEmpty {
                    textView.delegate?.textViewDidBeginEditing?(textView)
                    self.patient.controlledSubstances = textView.text!
                    textView.delegate?.textViewDidEndEditing?(textView)
                } else {
                    self.patient.controlledSubstances = nil
                    sender.isSelected = false
                }
                popUpView.removeFromSuperview()

            })
        } else {
            patient.controlledSubstances = nil
        }
    }
}

extension MedicalHistoryStep2ViewController : UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if tableView.tag == 1 {
            if radioButton.selected.tag == 1 {
                if indexPath.row == 3 {
                    patient.medicalHistoryQuestions2[0].selectedOption = false
                    patient.medicalHistoryQuestions2[1].selectedOption = false
                    patient.medicalHistoryQuestions2[2].selectedOption = false
                    
                    let obj = patient.medicalHistoryQuestions2[indexPath.row]
                    obj.selectedOption = obj.selectedOption == true ? false : true
                } else {
                    patient.medicalHistoryQuestions2[3].selectedOption = false
                    
                    let obj = patient.medicalHistoryQuestions2[indexPath.row]
                    obj.selectedOption = obj.selectedOption == true ? false : true
                }
            }
        } else {
            let obj = patient.medicalHistoryQuestions3[indexPath.row]
            if obj.question == "Others" {
                if obj.selectedOption == nil || obj.selectedOption == false {
                    PopupTextView.popUpView().showWithPlaceHolder("IF YES TYPE HERE", completion: { (popUpView, textView) in
                        if !textView.isEmpty {
                            textView.delegate?.textViewDidBeginEditing?(textView)
                            self.patient.othersTextForm3 = textView.text!
                            textView.delegate?.textViewDidEndEditing?(textView)
                            obj.selectedOption = true
                            self.tableViewAlergetic.reloadData()
                        } else {
                            obj.selectedOption = false
                            self.tableViewAlergetic.reloadData()
                        }
                        popUpView.removeFromSuperview()
                    })
                } else {
                    patient.othersTextForm3 = nil
                    obj.selectedOption = obj.selectedOption == true ? false : true
                }
            } else {
                obj.selectedOption = obj.selectedOption == true ? false : true
            }
        }
        tableView.reloadData()
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return tableView.tag == 1 ? 35.0 : 40.0
    }
}

extension MedicalHistoryStep2ViewController : UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return tableView.tag == 1 ? patient.medicalHistoryQuestions2.count : patient.medicalHistoryQuestions3.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if tableView.tag == 1 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "cellMedicalHistoryStep2", for: indexPath) as! MedicalHistoryStep1TableViewCell
            let obj = patient.medicalHistoryQuestions2[indexPath.row]
            cell.buttonCheck.isSelected = obj.selectedOption == true
            cell.buttonCheck.isEnabled = radioButton.isSelected
            cell.labelTitle.isEnabled = radioButton.isSelected
            cell.configureCheckCell(obj)
            return cell
        } else {
            let cell = tableView.dequeueReusableCell(withIdentifier: "cellMedicalHistoryStep3", for: indexPath) as! MedicalHistoryStep1TableViewCell
            let obj = patient.medicalHistoryQuestions3[indexPath.row]
            cell.buttonCheck.isSelected = obj.selectedOption == true
            cell.configureCheckCell(obj)
            return cell
        }
    }
}
