//
//  Liability1ViewController.swift
//  BlackWarrior
//
//  Created by Bala Murugan on 12/18/16.
//  Copyright © 2016 Bala Murugan. All rights reserved.
//

import UIKit

class Liability1ViewController: MCViewController {

    @IBOutlet weak var signaturePatient : SignatureView!
    @IBOutlet weak var labelDate : DateLabel!
    @IBOutlet weak var signatureWitness : SignatureView!
    @IBOutlet weak var labelDate1 : DateLabel!
    @IBOutlet weak var signatureDentist : SignatureView!
    @IBOutlet weak var labelDate2 : DateLabel!
    @IBOutlet weak var textfieldWitnessName : MCTextField!
    @IBOutlet weak var textfieldDentistName : MCTextField!
    @IBOutlet weak var labelDetails : UILabel!
    @IBOutlet weak var labelPatientName : UILabel!
    

    override func viewDidLoad() {
        super.viewDidLoad()

        labelDate.todayDate = patient.dateToday
        labelDate1.todayDate = patient.dateToday
        labelDate2.todayDate = patient.dateToday
        let str = labelDetails.text!
        let fee = patient.selectedForms.first!.toothNumbers
        labelDetails.text = str.replacingOccurrences(of: "KFEE", with: fee!)
        labelPatientName.text = patient.fullName
        loadValue()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func loadValue() {
       textfieldWitnessName.text = patient.witnessName
        textfieldDentistName.text = patient.liabilityDentistName
    }
    
    func saveValue()  {
        patient.witnessName = textfieldWitnessName.isEmpty ? "" : textfieldWitnessName.text!
        patient.liabilityDentistName = textfieldDentistName.isEmpty ? "" : textfieldDentistName.text!
    }

    @IBAction override func buttonBackAction() {
        saveValue()
        super.buttonBackAction()
    }

    @IBAction func onNextButtonPressed (withSender sender : UIButton){
        if textfieldDentistName.isEmpty || textfieldWitnessName.isEmpty{
            self.showAlert("PLEASE ENTER ALL THE REQUIRED FIELDS")
        }else if !signaturePatient.isSigned() || !signatureWitness.isSigned() || !signatureDentist.isSigned(){
            self.showAlert("PLEASE SIGN THE FORM")
        }else if !labelDate.dateTapped || !labelDate1.dateTapped || !labelDate2.dateTapped{
            self.showAlert("PLEASE SELECT THE DATE")
        }else{
            saveValue()
            let liability = consentStoryBoard.instantiateViewController(withIdentifier: "LiabilityFormVC") as! LiabilityFormViewController
            liability.patient = self.patient
            liability.signPatient = signaturePatient.signatureImage()
            liability.signWitness = signatureWitness.signatureImage()
            liability.signDentist = signatureDentist.signatureImage()
            self.navigationController?.pushViewController(liability, animated: true)
            
        }
    }

}
