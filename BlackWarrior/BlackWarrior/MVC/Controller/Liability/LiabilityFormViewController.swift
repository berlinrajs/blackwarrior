//
//  LiabilityFormViewController.swift
//  BlackWarrior
//
//  Created by Bala Murugan on 12/18/16.
//  Copyright © 2016 Bala Murugan. All rights reserved.
//

import UIKit

class LiabilityFormViewController: MCViewController {

    var signPatient : UIImage!
    var signWitness : UIImage!
    var signDentist : UIImage!
    @IBOutlet weak var labelPatientName : UILabel!
    @IBOutlet weak var labelDate : UILabel!
    @IBOutlet weak var patientSign : UIImageView!
    @IBOutlet weak var witnessSign : UIImageView!
    @IBOutlet weak var dentistSign : UIImageView!
    @IBOutlet weak var labelWitnessName : UILabel!
    @IBOutlet weak var labelDentistName : UILabel!
    @IBOutlet weak var labelDate1 : UILabel!
    @IBOutlet weak var labelDate2 : UILabel!
    @IBOutlet weak var labelDetails : UILabel!

    override func viewDidLoad() {
        super.viewDidLoad()

        labelPatientName.text = patient.fullName
        labelDate.text = patient.dateToday
        patientSign.image = signPatient
        witnessSign.image = signWitness
        dentistSign.image = signDentist
        labelWitnessName.text = patient.witnessName
        labelDentistName.text = patient.liabilityDentistName
        labelDate1.text = patient.dateToday
        labelDate2.text = patient.dateToday
        let str = labelDetails.text!
        let fee = patient.selectedForms.first!.toothNumbers
        labelDetails.text = str.replacingOccurrences(of: "KFEE", with: fee!)

    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    


}
