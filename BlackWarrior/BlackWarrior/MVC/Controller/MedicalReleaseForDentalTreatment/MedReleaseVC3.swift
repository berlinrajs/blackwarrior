//
//  PatientSignInStep2VC.swift
//  MConsentForms
//
//  Created by Berlin Raj on 22/09/16.
//  Copyright © 2016 SRS Web Solutions. All rights reserved.
//

import UIKit

class MedReleaseVC3: MCViewController {

      @IBOutlet weak var textViewMedicationRecommend: MCTextView!
      @IBOutlet weak var textViewOtherRecommend: MCTextView!
    @IBOutlet weak var textFieldPhone: MCTextField!
    @IBOutlet weak var textFieldFax: MCTextField!
    @IBOutlet weak var sign1: SignatureView!
    @IBOutlet weak var date1: DateLabel!
     @IBOutlet weak var radioIniated: RadioButton!
    var preMedication: String!
    override func viewDidLoad() {
        super.viewDidLoad()

        loadValues()
      textFieldPhone.textFormat = .phone
        textFieldFax.textFormat = .phone
        date1.todayDate = patient.dateToday
        // Do any additional setup after loading the view
    }

    @IBAction override func buttonBackAction() {
        saveValues()
        self.navigationController?.popViewController(animated: true)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    func saveValues(){
        
        patient.medicationRecomend = textViewMedicationRecommend.textValue
        patient.otherRecomend = textViewOtherRecommend.textValue
        patient.physicianPhone = textFieldPhone.text
        patient.physicianFax = textFieldFax.text
        patient.signature1 = sign1.signatureImage()
        
        patient.radioPreMed = radioIniated.selected.tag
        patient.preMedication = self.preMedication
    }
    
    func loadValues(){
      textViewMedicationRecommend.textValue =   patient.medicationRecomend
      textViewOtherRecommend.textValue =   patient.otherRecomend
        textFieldPhone.text = patient.physicianPhone
        textFieldFax.text = patient.physicianFax
        radioIniated.setSelectedWithTag(patient.radioPreMed)
        self.preMedication = patient.preMedication
    }
    
    @IBAction func radioActionPremed(_ sender: AnyObject) {
        
        if sender.tag == 1{
            PopupTextField.popUpView().showInViewController(self, WithTitle: "IF SO,", placeHolder: "SPECIFY HERE", textFormat: .default, completion: { (popUpView, textField) in
                if !textField.isEmpty{
                    self.preMedication = textField.text
                }else{
                    self.preMedication = ""
                    self.radioIniated.setSelectedWithTag(2)
                }
                popUpView.removeFromSuperview()
            })
        }else{
            self.preMedication = ""
        }
    }

    
    @IBAction func buttonNextAction() {
        self.view.endEditing(true)
       
        if !textFieldPhone.isEmpty && !textFieldPhone.text!.isPhoneNumber{
            self.showAlert("PLEASE ENTER VALID PHONE NUMBER")
        }else if !textFieldFax.isEmpty && !textFieldFax.text!.isPhoneNumber{
         self.showAlert("PLEASE ENTER VALID FAX NUMBER")
        }else if !sign1.isSigned(){
            self.showAlert("PLEASE SIGN THE FORM")
        }else if !date1.dateTapped{
            self.showAlert("PLEASE SELECT THE DATE")
        } else{
        
        saveValues()
            let nitrous = consentStoryBoard2.instantiateViewController(withIdentifier: "kMedReleaseVC4") as! MedReleaseVC4
            nitrous.patient = self.patient
            self.navigationController?.pushViewController(nitrous, animated: true)

        }
    }
    
}
