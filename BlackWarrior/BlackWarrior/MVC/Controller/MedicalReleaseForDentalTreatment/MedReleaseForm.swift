//
//  ApicoectomyForm.swift
//  SmileDesignDentalSpa
//
//  Created by APPLE on 12/5/16.
//  Copyright © 2016 srs. All rights reserved.
//

import UIKit

class MedReleaseForm: MCViewController {
    
    @IBOutlet weak var patientSign: UIImageView!
    @IBOutlet weak var physicianSign: UIImageView!
    @IBOutlet weak var specialistSign: UIImageView!
    @IBOutlet weak var patientName: UILabel!
    @IBOutlet weak var labelDate: UILabel!
    
    
    @IBOutlet weak var patientFullName: UILabel!
    @IBOutlet weak var examinedPatientName: UILabel!
    @IBOutlet weak var examinedPatientDate: UILabel!
    @IBOutlet weak var futureDate: UILabel!
    
    @IBOutlet var labelArray1: [UILabel]!
    @IBOutlet var labelArray2: [UILabel]!
    @IBOutlet var labelArray3: [UILabel]!
    @IBOutlet var labelArray4: [UILabel]!
    @IBOutlet var labelArray5: [UILabel]!
    @IBOutlet var labelArray6: [UILabel]!
     @IBOutlet weak var radioIniated: RadioButton!
    @IBOutlet weak var phone1: UILabel!
    @IBOutlet weak var phone2: UILabel!
    
    @IBOutlet weak var otherMedication: UILabel!
    @IBOutlet weak var fax1: UILabel!
    @IBOutlet weak var fax2: UILabel!
    
    @IBOutlet weak var labelDate1: UILabel!
    

    override func viewDidLoad() {
        super.viewDidLoad()
        
        physicianSign.image = patient.signature1
        specialistSign.image = patient.signature2
        patientSign.image = patient.signature3
        patientName.text = patient.fullName
        labelDate.text = patient.dateToday
        labelDate1.text = patient.dateToday
        
        patientFullName.text = patient.fullName
        examinedPatientName.text = patient.fullName
        examinedPatientDate.text = patient.patientDate
        futureDate.text = patient.futureDate
        
        patient.followingDentalTreatment.setTextForArrayOfLabels(labelArray1)
        patient.beforeProceding.setTextForArrayOfLabels(labelArray2)
        patient.orTakingFollowing.setTextForArrayOfLabels(labelArray3)
        patient.inYourOpinion.setTextForArrayOfLabels(labelArray4)
        
        patient.medicationRecomend.setTextForArrayOfLabels(labelArray5)
        patient.otherRecomend.setTextForArrayOfLabels(labelArray6)
        
       // radioIniated.setSelectedWithTag(patient.radioPreMed)
        otherMedication.text = patient.preMedication
        
        phone1.text = patient.physicianPhone
        phone2.text = patient.specialistPhone
        fax1.text = patient.physicianFax
        fax2.text = patient.specialistFax
        // Do any additional setup after loading the view....
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    


}
