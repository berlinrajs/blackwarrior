//
//  PatientSignInStep2VC.swift
//  MConsentForms
//
//  Created by Berlin Raj on 22/09/16.
//  Copyright © 2016 SRS Web Solutions. All rights reserved.
//

import UIKit

class MedReleaseVC2: MCViewController {

    @IBOutlet weak var textViewBeforProceding: MCTextView!
      @IBOutlet weak var textViewOrFollowing: MCTextView!
      @IBOutlet weak var textViewInYourOpinion: MCTextView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        loadValues()
      
        // Do any additional setup after loading the view
    }

    @IBAction override func buttonBackAction() {
        saveValues()
        self.navigationController?.popViewController(animated: true)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    func saveValues(){
        
        patient.beforeProceding = textViewBeforProceding.textValue
        patient.orTakingFollowing = textViewOrFollowing.textValue
        patient.inYourOpinion = textViewInYourOpinion.textValue
    }
    
    func loadValues(){
       textViewBeforProceding.textValue =  patient.beforeProceding
      textViewOrFollowing.textValue =   patient.orTakingFollowing
      textViewInYourOpinion.textValue =   patient.inYourOpinion 
    }
    
    @IBAction func buttonNextAction() {
        self.view.endEditing(true)
       
        saveValues()
        let nitrous = consentStoryBoard2.instantiateViewController(withIdentifier: "kMedReleaseVC3") as! MedReleaseVC3
        nitrous.patient = self.patient
        self.navigationController?.pushViewController(nitrous, animated: true)

        
    }
    
}
