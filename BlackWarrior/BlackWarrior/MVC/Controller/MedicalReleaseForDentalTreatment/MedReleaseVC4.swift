//
//  PatientSignInStep2VC.swift
//  MConsentForms
//
//  Created by Berlin Raj on 22/09/16.
//  Copyright © 2016 SRS Web Solutions. All rights reserved.
//

import UIKit

class MedReleaseVC4: MCViewController {

    @IBOutlet weak var textFieldPhone: MCTextField!
    @IBOutlet weak var textFieldFax: MCTextField!
    @IBOutlet weak var sign1: SignatureView!
    @IBOutlet weak var date1: DateLabel!
    @IBOutlet weak var sign2: SignatureView!
    @IBOutlet weak var date2: DateLabel!
    @IBOutlet weak var patientSignName: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        loadValues()
      textFieldPhone.textFormat = .phone
        textFieldFax.textFormat = .phone
        date1.todayDate = patient.dateToday
        date2.todayDate = patient.dateToday
        patientSignName.text = patient.fullName
        // Do any additional setup after loading the view
    }

    @IBAction override func buttonBackAction() {
        saveValues()
        self.navigationController?.popViewController(animated: true)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    func saveValues(){
        patient.specialistPhone = textFieldPhone.text
        patient.specialistFax = textFieldFax.text
        patient.signature2 = sign1.signatureImage()
        patient.signature3 = sign2.signatureImage()
    }
    
    func loadValues(){
        textFieldPhone.text = patient.specialistPhone
        textFieldFax.text = patient.specialistFax
        }
    
    @IBAction func buttonNextAction() {
        self.view.endEditing(true)
       
        if !textFieldPhone.isEmpty && !textFieldPhone.text!.isPhoneNumber{
            self.showAlert("PLEASE ENTER VALID PHONE NUMBER")
        }else if !textFieldFax.isEmpty && !textFieldFax.text!.isPhoneNumber{
         self.showAlert("PLEASE ENTER VALID FAX NUMBER")
        }else if !sign1.isSigned() || !sign2.isSigned(){
            self.showAlert("PLEASE SIGN THE FORM")
        }else if !date1.dateTapped || !date2.dateTapped{
            self.showAlert("PLEASE SELECT THE DATE")
        } else{
        
        saveValues()
            let nitrous = consentStoryBoard2.instantiateViewController(withIdentifier: "kMedReleaseForm") as! MedReleaseForm
            nitrous.patient = self.patient
            self.navigationController?.pushViewController(nitrous, animated: true)
        }
    }
    
}
