//
//  PatientSignInStep2VC.swift
//  MConsentForms
//
//  Created by Berlin Raj on 22/09/16.
//  Copyright © 2016 SRS Web Solutions. All rights reserved.
//

import UIKit

class MedReleaseVC1: MCViewController {

    @IBOutlet weak var textFieldFutureTreatmentDate: MCTextField!
    @IBOutlet weak var textFieldPateintDate: MCTextField!
    @IBOutlet weak var tetViewFollowingDentalTreatment: MCTextView!
    override func viewDidLoad() {
        super.viewDidLoad()

        loadValues()
      textFieldFutureTreatmentDate.textFormat = .dateInCurrentYear
        textFieldPateintDate.textFormat = .dateInCurrentYear
        // Do any additional setup after loading the view
    }

    @IBAction override func buttonBackAction() {
        saveValues()
        self.navigationController?.popViewController(animated: true)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    func saveValues(){
        
        patient.futureDate = textFieldFutureTreatmentDate.text
        patient.patientDate = textFieldPateintDate.text
        patient.followingDentalTreatment = tetViewFollowingDentalTreatment.textValue
        
    }
    
    func loadValues(){
        
      textFieldFutureTreatmentDate.text =   patient.futureDate
       textFieldPateintDate.text =  patient.patientDate
        tetViewFollowingDentalTreatment.textValue =  patient.followingDentalTreatment
    }
    
    @IBAction func buttonNextAction() {
        self.view.endEditing(true)
       
        saveValues()
            let nitrous = consentStoryBoard2.instantiateViewController(withIdentifier: "kMedReleaseVC2") as! MedReleaseVC2
            nitrous.patient = self.patient
            self.navigationController?.pushViewController(nitrous, animated: true)
         
    }
    
}
