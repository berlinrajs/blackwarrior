//
//  HomeViewController.swift
//  AdultMedicalForm
//
//  Created by SRS Web Solutions on 06/05/16.
//  Copyright © 2016 SRS. All rights reserved.
//

import UIKit

class HomeViewControllerAuto: MCViewController {
    @IBOutlet var lblDate: UILabel!
    
    @IBOutlet weak var tableViewForms: UITableView!
    
    @IBOutlet weak var labelVersion: UILabel!
    @IBOutlet weak var labelNetwork: UILabel!
 
    @IBOutlet weak var constraintTableViewTop: NSLayoutConstraint!
    @IBOutlet weak var constraintTableViewBottom: NSLayoutConstraint!
    
    var isNewPatient: Bool! = false

    var selectedForms : [Forms]! = [Forms]()
    var formList : [Forms]! = [Forms]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        if let text = Bundle.main.infoDictionary?[kCFBundleVersionKey as String] as? String {
            labelVersion.text = text
        }
        self.navigationController?.navigationBar .isHidden = true
        NotificationCenter.default.addObserver(self, selector: #selector(showAlertPopup), name: NSNotification.Name(rawValue: kFormsCompletedNotification), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(dateChangedNotification), name: NSNotification.Name(rawValue: kDateChangedNotification), object: nil)

        self.dateChangedNotification()
        
    }
    
    
    func internetStatusChanges() {
        if !Reachability.isConnectedToNetwork(){
            DispatchQueue.main.async(execute: {
                self.labelNetwork.text = "Internet : \n OFF"

            })
        }else{
            self.labelNetwork.text = "Internet : \n ON"

        }
        self.perform(#selector(HomeViewControllerAuto.internetStatusChanges), with: nil, afterDelay: 5.0)
    }

    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(true)
        NSObject.cancelPreviousPerformRequests(withTarget: self)
    }
    override func viewWillAppear(_ animated: Bool) {
        self.internetStatusChanges()
        super.viewWillAppear(animated)
        DispatchQueue.global(qos: DispatchQoS.QoSClass.background).async {
            let defaults = UserDefaults.standard
            ServiceManager.loginWithUsername(defaults.value(forKey: kAppLoginUsernameKey) as! String, password: defaults.value(forKey: kAppLoginPasswordKey) as! String) { (success, error) -> (Void) in
                if success {
                    
                } else {
                    if error == nil {
                        
                    } else {
                        DispatchQueue.main.async(execute: {
                            UserDefaults.standard.set(false, forKey: "kApploggedIn")
                            UserDefaults.standard.synchronize()
                            (UIApplication.shared.delegate as! AppDelegate).checkAutologin()
                        })
                    }
                }
            }
        }
        Forms.getAllForms { (isConnectionFailed, forms) -> Void in
            self.formList = forms
            self.tableViewForms.reloadData()
            if isConnectionFailed == true {
                let alertController = UIAlertController(title: kAppName, message: "Your device is not connected to internet. Please go to settings to connect.", preferredStyle: UIAlertControllerStyle.alert)
                let alertOkAction = UIAlertAction(title: "Settings", style: UIAlertActionStyle.destructive) { (action) -> Void in
                    let settingsUrl = NSURL(string: UIApplicationOpenSettingsURLString)
                    if let url = settingsUrl {
                        UIApplication.shared.openURL(url as URL)
                    }
                }
                let alertCancelAction = UIAlertAction(title: "Cancel", style: UIAlertActionStyle.destructive) { (action) -> Void in
                    
                }
                alertController.addAction(alertOkAction)
                alertController.addAction(alertCancelAction)
                self.present(alertController, animated: true, completion: nil)
            }
        }
    }
    
    func dateChangedNotification() {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "MMM dd, yyyy"
        self.lblDate.text = dateFormatter.string(from: NSDate() as Date).uppercased()
    }

    
    
    @IBAction func btnNextAction(sender: AnyObject) {
        selectedForms.removeAll()
        for (_, form) in formList.enumerated() {
            if form.isSelected == true {
//                if form.formTitle == kConsentForms  {
                    for subForm in form.subForms {
                        if subForm.isSelected == true {
                            selectedForms.append(subForm)
                        }
                    }
//                } else {
//                    selectedForms.append(form)
//                }
            }
        }
        self.view.endEditing(true)
        
        if selectedForms.count > 0 {
//            selectedForms.sortInPlace({ (formObj1, formObj2) -> Bool in
//                return formObj1.index < formObj2.index
//            })
            self.view.layoutIfNeeded()
            constraintTableViewTop.constant = 100
            UIView.animate(withDuration: 0.2, animations: {
                self.view.layoutIfNeeded()
            }) { (finished) in
                self.constraintTableViewBottom.constant = 201
            }
            let patient = MCPatient(forms: selectedForms)
            patient.dateToday = lblDate.text
            let patientInfoVC = self.storyboard?.instantiateViewController(withIdentifier: "kPatientInfoVC") as! PatientInfoViewController
            patientInfoVC.patient = patient
            patientInfoVC.isNewPatient = self.isNewPatient
            self.navigationController?.pushViewController(patientInfoVC, animated: true)
        } else {
            showAlert("PLEASE SELECT ANY FORM")
        }
    }

    
    
    
    func showAlertPopup() {
        self.showCustomAlert("PLEASE HANDOVER THE DEVICE BACK TO FRONT DESK")

    }

    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
}


extension HomeViewControllerAuto : UITableViewDelegate {
    
    func tapGestureDidSelectAction(sender : UITapGestureRecognizer) {
        let indexPath = (sender.view as! FormsTableViewCell).indexPath
        let form = formList[(indexPath?.section)!].subForms[(indexPath?.row)!]
        form.isSelected = !form.isSelected
        if indexPath?.section == 0 {
            if formList[0].subForms[0].isSelected == false {
                for form in formList[0].subForms {
                    form.isSelected = false
                }
            }
            tableViewForms.reloadSections(NSIndexSet(index: 0) as IndexSet, with: UITableViewRowAnimation.none)
            if form.isSelected && form.formTitle == kNewPatientSignInForm {
                PopupTextField.popUpView().showInViewController(self, WithTitle: "CHART NUMBER", placeHolder: "PLEASE SPECIFY", textFormat: TextFormat.numbersWithoutValidation, completion: { (popUpView, textField) in
                    popUpView.close()
                    form.toothNumbers = textField.text
                    self.tableViewForms.reloadRows(at: [indexPath!], with: .fade)
                })
            }
            return
        }
        
        tableViewForms.reloadRows(at: [indexPath!], with: .fade)
        
        if form.isSelected && form.formTitle == kLiability{
            PopupTextField.popUpView().showInViewController(self, WithTitle: "ENTER THE FEE", placeHolder: "FEE", textFormat: TextFormat.amount, completion: { (popUpView, textField) in
                if textField.isEmpty{
                    form.isSelected = false
                }else{
                    form.toothNumbers = textField.text
                }
                popUpView.close()
                self.tableViewForms.reloadRows(at: [indexPath!], with: .fade)
            })
        }
    }
    
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cellSubForm") as! FormsTableViewCell
        let form = formList[indexPath.section].subForms[indexPath.row]
        let height = form.formTitle.heightWithConstrainedWidth(392.0, font: cell.labelFormName.font) + 20
        return height
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 102.0
    }
    
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let form = formList[section]
        let cell = tableView.dequeueReusableCell(withIdentifier: "cellHeader") as! FormsTableViewCell
        let headerView = cell.contentView.subviews[1] as! PDTableHeaderView
        headerView.labelFormName.text = form.formTitle
        
        let selectedForm = formList.filter { (obj) -> Bool in
            return obj.isSelected == true
        }
         DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 0.2) {
            if selectedForm.count > 0 {
                UIView.animate(withDuration: 0.3) {
                    cell.contentView.alpha = form.isSelected == false ? 0.2 : 1.0
                }
            } else {
                UIView.animate(withDuration: 0.3) {
                    cell.contentView.alpha = 1.0
                }
            }
        }
        
        
        headerView.tag = section
        if headerView.gestureRecognizers == nil {
            let tapGesture = UITapGestureRecognizer(target: self, action: #selector(tapGestureAction))
            headerView.addGestureRecognizer(tapGesture)
        }
        return cell.contentView
    }
    
    func tapGestureAction(sender : UITapGestureRecognizer) {
        
        let headerView = sender.view as! PDTableHeaderView
        let form = self.formList[headerView.tag]
        
        let selectedForm = formList.filter { (obj) -> Bool in
            return obj.isSelected == true
        }
        
        func displayRows() {
            self.view.layoutIfNeeded()
            constraintTableViewTop.constant = 30
            UIView.animate(withDuration: 0.3, animations: {
                self.view.layoutIfNeeded()
            }) { (finished) in
                self.constraintTableViewBottom.constant = 30
                self.isNewPatient = form.formTitle == kNewPatient.keys.first ? true : false
                form.isSelected = !form.isSelected
                var indexPaths : [NSIndexPath] = [NSIndexPath]()
                for (idx, _) in form.subForms.enumerated() {
                    let indexPath = NSIndexPath(row: idx, section: headerView.tag)
                    indexPaths.append(indexPath)
                }
                self.tableViewForms.beginUpdates()
                self.tableViewForms.insertRows(at: indexPaths as [IndexPath], with: .fade)
                self.tableViewForms.endUpdates()
                DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 0.1, execute: {
                    self.tableViewForms.scrollToRow(at: indexPaths.last! as IndexPath, at: .bottom, animated: true)
                    DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 0.1, execute: {
                        self.tableViewForms.isUserInteractionEnabled = true
                        self.tableViewForms.reloadData()
                    })
                })
            }
        }
        
        self.tableViewForms.isUserInteractionEnabled = false
        if selectedForm.count > 0 {
            selectedForm[0].isSelected = false
            let section = selectedForm[0].formTitle == kNewPatient.keys.first ? 0 : selectedForm[0].formTitle == kExistingPatient.keys.first ? 1 : 2
            var indexPaths : [NSIndexPath] = [NSIndexPath]()
            
            for (idx, _) in selectedForm[0].subForms.enumerated() {
                let indexPath = NSIndexPath(row: idx, section: section)
                indexPaths.append(indexPath)
            }
            if form.formTitle == kNewPatient.keys.first {
                for obj in self.formList {
                    for subForm in obj.subForms {
                        subForm.isSelected = false
                    }
                }
            }
            self.tableViewForms.beginUpdates()
            self.tableViewForms.deleteRows(at: indexPaths as [IndexPath], with: .none)
            self.tableViewForms.endUpdates()
            
            self.view.layoutIfNeeded()
            constraintTableViewTop.constant = 100
            UIView.animate(withDuration: 0.3, animations: {
                self.view.layoutIfNeeded()
            }) { (finished) in
                self.constraintTableViewBottom.constant = 201
                if form.formTitle != selectedForm[0].formTitle {
                    DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 0.3, execute: {
                        displayRows()
                    })
                } else {
                    DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 0.1, execute: {
                        self.tableViewForms.isUserInteractionEnabled = true
                        self.tableViewForms.reloadData()
                    })

                }
            }
        } else {
            displayRows()
        }
    }
}

extension HomeViewControllerAuto : UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int  {
        return formList.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        let form = formList[section]
        return form.isSelected == true ? form.subForms.count : 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cellSubForm", for: indexPath) as! FormsTableViewCell
        let form = formList[indexPath.section].subForms[indexPath.row]
        cell.labelFormName.text = form.formTitle
        cell.imageViewCheckMark.isHidden = !form.isSelected
        cell.labelFormName.textColor = form.isSelected == true ? UIColor(red: 19/255.0, green: 192/255.0, blue: 235/255.0, alpha: 1.0) : UIColor.white
        cell.indexPath = indexPath
        if indexPath.section == 0 && indexPath.row > 0 {
            cell.labelFormName.textColor = formList[0].subForms[0].isSelected == true ? (form.isSelected == true ? UIColor(red: 19/255.0, green: 192/255.0, blue: 235/255.0, alpha: 1.0) : UIColor.white) : UIColor.lightGray
        } else {
            cell.labelFormName.textColor = form.isSelected == true ? UIColor(red: 19/255.0, green: 192/255.0, blue: 235/255.0, alpha: 1.0) : UIColor.white
        }
        
        if cell.gestureRecognizers == nil {
            let tapGesture = UITapGestureRecognizer(target: self, action: #selector(tapGestureDidSelectAction))
            cell.addGestureRecognizer(tapGesture)
        }
        cell.backgroundColor = UIColor.clear
        cell.contentView.backgroundColor = UIColor.clear
        return cell
    }
}

extension HomeViewControllerAuto : UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        return true
    }
}
