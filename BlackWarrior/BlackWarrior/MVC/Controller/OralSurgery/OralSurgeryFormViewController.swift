//
//  OralSurgeryFormViewController.swift
//  BlackWarrior
//
//  Created by Bala Murugan on 12/20/16.
//  Copyright © 2016 Bala Murugan. All rights reserved.
//

import UIKit

class OralSurgeryFormViewController: MCViewController {

    @IBOutlet weak var labelPatientName : UILabel!
    @IBOutlet weak var labelDate1 : UILabel!
    @IBOutlet weak var labelProcedure : UILabel!
    @IBOutlet      var labelComments : [UILabel]!
    @IBOutlet weak var signaturePatient : UIImageView!
    @IBOutlet weak var labelDate2 : UILabel!
    @IBOutlet weak var labelRepresentativeName : UILabel!
    @IBOutlet weak var labelRepresentativeRelation : UILabel!
    @IBOutlet weak var labelDate3 : UILabel!
    @IBOutlet weak var signatureDoctor : UIImageView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        labelPatientName.text = patient.fullName
        labelDate1.text = patient.dateToday
        labelProcedure.text = patient.oralProcedure
        patient.oralComments.setTextForArrayOfLabels(labelComments)
        signaturePatient.image = patient.oralPatientSign
        labelDate2.text = patient.dateToday
        labelRepresentativeName.text = patient.oralRepresentativeName
        labelRepresentativeRelation.text = patient.oralRepresentativeRelationship
        labelDate3.text = patient.dateToday
        signatureDoctor.image = patient.oralDoctorSign
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    


}
