//
//  OralSurgery1ViewController.swift
//  BlackWarrior
//
//  Created by Bala Murugan on 12/20/16.
//  Copyright © 2016 Bala Murugan. All rights reserved.
//

import UIKit

class OralSurgery1ViewController: MCViewController {

    @IBOutlet weak var textfieldProcedure : MCTextField!
    @IBOutlet weak var textviewComments : MCTextView!
    @IBOutlet weak var textfieldRepresentativeName : MCTextField!
    @IBOutlet weak var textfieldRepresentativeRelationship : MCTextField!
    @IBOutlet weak var viewRepresentative : UIView!
    @IBOutlet weak var radioSignature : RadioButton!
    @IBOutlet weak var labelSignatureTitle : UILabel!
    @IBOutlet weak var labelPatientName : UILabel!
    @IBOutlet weak var signaturePatient : SignatureView!
    @IBOutlet weak var signatureDoctor : SignatureView!
    @IBOutlet weak var labelDate1 : DateLabel!
    @IBOutlet weak var labelDate2 : DateLabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        loadValue()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func loadValue()  {
        textviewComments.placeholder = "PLEASE TYPE HERE"
        textviewComments.placeholderColor = UIColor.lightGray
        labelDate1.todayDate = patient.dateToday
        labelDate2.todayDate = patient.dateToday
        
        textfieldProcedure.text = patient.oralProcedure
        textviewComments.text = patient.oralComments == "" ? "PLEASE TYPE HERE" : patient.oralComments
        textviewComments.textColor = patient.oralComments == "" ? UIColor.lightGray : UIColor.black
        textfieldRepresentativeName.text = patient.oralRepresentativeName
        textfieldRepresentativeRelationship.text = patient.oralRepresentativeRelationship
        radioSignature.setSelectedWithTag(patient.radioSignTag)
        if patient.radioSignTag == 1{
            viewRepresentative.isUserInteractionEnabled = false
            viewRepresentative.alpha = 0.5
            labelSignatureTitle.text = "Patient Signature"
            labelPatientName.text = patient.fullName
        }else{
            viewRepresentative.isUserInteractionEnabled = true
            viewRepresentative.alpha = 1.0
            labelSignatureTitle.text = "Legally Authorized Representative Signature"
            labelPatientName.text = ""

        }
        
    }
    
    func saveValue()  {
        patient.oralProcedure = textfieldProcedure.isEmpty ? "" : textfieldProcedure.text!
        patient.oralComments = textviewComments.isEmpty ? "" : textviewComments.text!
        patient.oralRepresentativeName = textfieldRepresentativeName.isEmpty ? "" : textfieldRepresentativeName.text!
        patient.oralRepresentativeRelationship = textfieldRepresentativeRelationship.isEmpty ? "" : textfieldRepresentativeRelationship.text!
        patient.radioSignTag = radioSignature.selected.tag
        
        
    }
    
    @IBAction override func buttonBackAction() {
        saveValue()
        super.buttonBackAction()
    }
    
    @IBAction func onRadioSignaturePressed (withSender sender : RadioButton){
        if sender.tag == 1{
            viewRepresentative.isUserInteractionEnabled = false
            viewRepresentative.alpha = 0.5
            labelSignatureTitle.text = "Patient Signature"
            labelPatientName.text = patient.fullName
            textfieldRepresentativeName.text = ""
            textfieldRepresentativeRelationship.text = ""
        }else{
            viewRepresentative.isUserInteractionEnabled = true
            viewRepresentative.alpha = 1.0
            labelSignatureTitle.text = "Legally Authorized Representative Signature"
            labelPatientName.text = ""
        }
    }
    
    @IBAction func onNextButtonPressed (withSender sender : UIButton){
        if textfieldProcedure.isEmpty {
            self.showAlert("PLEASE ENTER ALL THE REQUIRED FIELDS")
        }else if radioSignature.selected.tag == 2 && (textfieldRepresentativeName.isEmpty || textfieldRepresentativeRelationship.isEmpty){
            self.showAlert("PLEASE ENTER ALL THE REQUIRED FIELDS")
        }else if !signaturePatient.isSigned() || !signatureDoctor.isSigned(){
            self.showAlert("PLEASE SIGN THE FORM")
        }else if !labelDate1.dateTapped || !labelDate2.dateTapped{
            self.showAlert("PLEASE SELECT THE DATE")
        }else{
            saveValue()
            patient.oralPatientSign = signaturePatient.signatureImage()
            patient.oralDoctorSign = signatureDoctor.signatureImage()
            let oral = consentStoryBoard.instantiateViewController(withIdentifier: "OralSurgeryFormVC") as! OralSurgeryFormViewController
            oral.patient = self.patient
            self.navigationController?.pushViewController(oral, animated: true)
        }
    }


}
