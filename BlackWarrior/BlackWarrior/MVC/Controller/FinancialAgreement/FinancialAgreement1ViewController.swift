//
//  FinancialAgreement1ViewController.swift
//  BlackWarrior
//
//  Created by Bala Murugan on 12/16/16.
//  Copyright © 2016 Bala Murugan. All rights reserved.
//

import UIKit

class FinancialAgreement1ViewController: MCViewController {

    @IBOutlet weak var textfieldResponsibleName : MCTextField!
    @IBOutlet weak var signaturePatient : SignatureView!
    @IBOutlet weak var labelDate : DateLabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        labelDate.todayDate = patient.dateToday
        loadValue()

    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func loadValue()  {
        textfieldResponsibleName.text = patient.responsiblePartyName
    }
    
    func saveValue (){
        patient.responsiblePartyName = textfieldResponsibleName.isEmpty ? "" : textfieldResponsibleName.text!
    }

    @IBAction override func buttonBackAction() {
        saveValue()
        super.buttonBackAction()
    }

    @IBAction func onNextButtonPressed (withSender sender : UIButton){
        if !signaturePatient.isSigned(){
            self.showAlert("PLEASE SIGN THE FORM")
        }else if !labelDate.dateTapped{
            self.showAlert("PLEASE SELECT THE DATE")
        }else{
            saveValue()
            let finance = consentStoryBoard.instantiateViewController(withIdentifier: "FinanceFormVC") as! FinancialAgreementFormViewController
            finance.patient = self.patient
            finance.signPatient = signaturePatient.signatureImage()
            self.navigationController?.pushViewController(finance, animated: true)

        }
    }

}
