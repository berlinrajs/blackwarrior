//
//  FinancialAgreementFormViewController.swift
//  BlackWarrior
//
//  Created by Bala Murugan on 12/16/16.
//  Copyright © 2016 Bala Murugan. All rights reserved.
//

import UIKit

class FinancialAgreementFormViewController: MCViewController {

    var signPatient : UIImage!
    @IBOutlet weak var labelResponsiblePartyName : UILabel!
    @IBOutlet weak var labelPatientName : UILabel!
    @IBOutlet weak var labelDate : UILabel!
    @IBOutlet weak var patientSign : UIImageView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        labelResponsiblePartyName.text = patient.responsiblePartyName
        labelPatientName.text = patient.fullName
        labelDate.text = patient.dateToday
        patientSign.image = signPatient
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    


}
