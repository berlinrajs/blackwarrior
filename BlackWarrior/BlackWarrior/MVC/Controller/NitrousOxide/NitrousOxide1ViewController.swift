//
//  NitrousOxide1ViewController.swift
//  BlackWarrior
//
//  Created by Bala Murugan on 12/17/16.
//  Copyright © 2016 Bala Murugan. All rights reserved.
//

import UIKit

class NitrousOxide1ViewController: MCViewController {

    @IBOutlet weak var signaturePatient : SignatureView!
    @IBOutlet weak var labelDate : DateLabel!
    @IBOutlet weak var labelPatientName : UILabel!

    override func viewDidLoad() {
        super.viewDidLoad()

        labelDate.todayDate = patient.dateToday
        labelPatientName.text = patient.fullName
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    @IBAction func onNextButtonPressed (withSender sender : UIButton){
        if !signaturePatient.isSigned() {
            self.showAlert("PLEASE SIGN THE FORM")
        }else if !labelDate.dateTapped {
            self.showAlert("PLEASE SELECT THE DATE")
        }else{
            let nitrous = consentStoryBoard.instantiateViewController(withIdentifier: "NitrousOxideFormVC") as! NitrousOxideFormViewController
            nitrous.patient = self.patient
            nitrous.signPatient = signaturePatient.signatureImage()
            self.navigationController?.pushViewController(nitrous, animated: true)
            
        }
    }

}
