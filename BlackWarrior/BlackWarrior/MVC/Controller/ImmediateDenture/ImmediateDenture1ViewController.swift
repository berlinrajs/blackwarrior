//
//  ImmediateDenture1ViewController.swift
//  BlackWarrior
//
//  Created by Bala Murugan on 12/16/16.
//  Copyright © 2016 Bala Murugan. All rights reserved.
//

import UIKit

class ImmediateDenture1ViewController: MCViewController {

    @IBOutlet weak var signaturePatient : SignatureView!
    @IBOutlet weak var labelDate : DateLabel!
    @IBOutlet weak var signatureWitness : SignatureView!
    @IBOutlet weak var labelDate1 : DateLabel!

    override func viewDidLoad() {
        super.viewDidLoad()

        labelDate.todayDate = patient.dateToday
        labelDate1.todayDate = patient.dateToday
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    @IBAction func onNextButtonPressed (withSender sender : UIButton){
        if !signaturePatient.isSigned() || !signatureWitness.isSigned(){
            self.showAlert("PLEASE SIGN THE FORM")
        }else if !labelDate.dateTapped || !labelDate1.dateTapped{
            self.showAlert("PLEASE SELECT THE DATE")
        }else{
            let immediate = consentStoryBoard.instantiateViewController(withIdentifier: "ImmediateDentureFormVC") as! ImmediateDentureFormViewController
            immediate.patient = self.patient
            immediate.signPatient = signaturePatient.signatureImage()
            immediate.signWitness = signatureWitness.signatureImage()
            self.navigationController?.pushViewController(immediate, animated: true)
        }
    }

}
