//
//  ImmediateDentureFormViewController.swift
//  BlackWarrior
//
//  Created by Bala Murugan on 12/16/16.
//  Copyright © 2016 Bala Murugan. All rights reserved.
//

import UIKit

class ImmediateDentureFormViewController: MCViewController {

    var signPatient : UIImage!
    var signWitness : UIImage!
    @IBOutlet weak var labelPatientName : UILabel!
    @IBOutlet weak var labelDate : UILabel!
    @IBOutlet weak var patientSign : UIImageView!
    @IBOutlet weak var witnessSign : UIImageView!

    override func viewDidLoad() {
        super.viewDidLoad()

        labelPatientName.text = patient.fullName
        labelDate.text = patient.dateToday
        patientSign.image = signPatient
        witnessSign.image = signWitness
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

}
