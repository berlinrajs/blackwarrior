//
//  ArtificialDenturesFormViewController.swift
//  BlackWarrior
//
//  Created by Bala Murugan on 12/17/16.
//  Copyright © 2016 Bala Murugan. All rights reserved.
//

import UIKit

class ArtificialDenturesFormViewController: MCViewController {

    var signPatient : UIImage!
    var signWitness : UIImage!
    @IBOutlet weak var labelPatientName : UILabel!
    @IBOutlet weak var labelDate : UILabel!
    @IBOutlet weak var patientSign : UIImageView!
    @IBOutlet weak var witnessSign : UIImageView!

    override func viewDidLoad() {
        super.viewDidLoad()

        labelPatientName.text = patient.fullName
        labelDate.text = patient.dateToday
        patientSign.image = signPatient
        witnessSign.image = signWitness
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
