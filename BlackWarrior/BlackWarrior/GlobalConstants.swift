//
//  Constants.swift
//  MConsentForms
//
//  Created by Berlin Raj on 20/09/16.
//  Copyright © 2016 SRS Web Solutions. All rights reserved.
//

public enum TextFormat : Int {
    case `default` = 0
    case socialSecurity
    case toothNumber
    case phone
    case extensionCode
    case zipcode
    case number
    case date
    case month
    case year
    case dateInCurrentYear
    case dateIn1980
    case time
    case middleInitial
    case state
    case amount
    case email
    case secureText
    case alphaNumeric
    case numbersWithoutValidation

}

//KEYS
let kFormsCompletedNotification = "kAllFormsCompletedSuccessfully"
let kDateChangedNotification = "kDateChangedToNextDateNotificaion"

let kAppLoggedInKey = "kApploggedIn"
let kAppLoginUsernameKey = "kAppLoginUserName"
let kAppLoginPasswordKey = "kAppLoginPassword"

//VARIABLES
#if AUTO
let kKeychainItemLoginName = "Black Warrior Dental Center Auto: Google Login"
let kKeychainItemName = "Black Warrior Dental Center Auto: Google Drive"
let kClientId = "1032887395853-d7gesukfe6fvibj3umkvcbg1ujf2ulbg.apps.googleusercontent.com"
let kClientSecret = "jtIWNPf7RJksY-iWNzmj914x"
let kFolderName = "BlackWarriorDentalCenterAuto"
#else
let kKeychainItemLoginName = "Black Warrior Dental Center: Google Login"
let kKeychainItemName = "Black Warrior Dental Center: Google Drive"
let kClientId = "485255812765-bclhe3t4cgo7htku78f5v792bgubstqe.apps.googleusercontent.com"
let kClientSecret = "VSecre08KE3BbV2JxfBYJYTd"
let kFolderName = "BlackWarriorDentalCenter"
#endif
    


let kDentistNames: [String] = ["DR. DENTIST NAME", "DR. DENTIST NAME", "DR. DENTIST NAME", "DR. DENTIST NAME", "DR. DENTIST NAME","DR. DENTIST NAME"]
let kDentistNameNeededForms = [kNewPatientSignInForm, kPatientSignInForm]
let kClinicName = "BLACK WARRIOR DENTAL CENTER"
let kAppName = "BLACK WARRIOR DENTAL CENTER"
let kPlace = "TUSCALOOSA, AL"
let kState = "AL"
let kAppKey = "mcBlackWarrior"
let kAppLoginAvailable: Bool = true
let kCommonDateFormat = "MMM dd, yyyy"

let kGoogleID = "demo@srswebsolutions.com"
let kGooglePassword = "Srsweb123#"
let kOutputFileMustBeImage: Bool = false
//END OF VARIABLES

//FORMS
let kNewPatientSignInForm = "NEW PATIENT SIGN IN FORM"
let kPatientSignInForm = "PATIENT SIGN IN FORM"

let kMedicalHistory = "MEDICAL HISTORY FORM"

//CONSENT FORMS
let kConsentForms = "CONSENT FORMS"
let kFinancialAgreement = "FINANCIAL AGREEMENT FOR BLACK WARRIOR DENTAL CENTER"
let kAppointment = "LATE AND MISSED APPOINTMENT POLICY"
let kImmediateDenture = "IMMEDIATE COMPLETE DENTURES AND PARTIAL DENTURES"
let kArtificialDenture = "FULL ARTIFICIAL DENTURES AND PARTIAL DENTURES"
let kNitrousOxide = "NITROUS OXIDE INFORMED CONSENT"
let kPeriodontal = "PERIODONTAL PROCEDURES SCALING AND ROOT PLANING"
let kEndodontic = "ENDODONTIC INFORMATION AND CONSENT FORM"
let kLiability = "LIABILITY RELEASE FORM"
let kSchoolExcuse = "SCHOOL EXCUSE FOR REASON OF DENTAL APPOINTMENT"
let kSatisfaction = "PATIENT SATISFACTION SURVEY"
let kReferral = "REFERRAL FORM"
let kAuthorization = "AUTHORIZATION FOR USE OF DISCLOSURE OF PROTECTED HEALTH INFORMATION"
let kMedicalRelease = "MEDICAL RELEASE FOR DENTAL TREATMENT"
let kOralSurgery = "CONSENT FOR ORAL SURGERY"

let kInsuranceCard = "SCAN INSURANCE CARD"
let kDrivingLicense = "SCAN DRIVING LICENSE"
let kSelfieForm = "PATIENT PHOTO"

#if AUTO
let kNewPatient = ["NEW PATIENT" : [kNewPatientSignInForm, kMedicalHistory, kInsuranceCard, kDrivingLicense,kSelfieForm,kFinancialAgreement,kAppointment,kAuthorization]]
let kExistingPatient = ["EXISTING PATIENT": [kPatientSignInForm, kMedicalHistory, kInsuranceCard, kDrivingLicense,kSelfieForm,kFinancialAgreement,kAppointment,kAuthorization]]
let kConsentFormsAuto = ["CONSENT FORMS": [kImmediateDenture,kArtificialDenture,kNitrousOxide,kPeriodontal,kEndodontic,kLiability,kSchoolExcuse,kMedicalRelease,kOralSurgery,kReferral,kSatisfaction]]
#endif

//let kFeedBack = "CUSTOMER REVIEW FORM"
//let kVisitorCheckForm = "VISITOR CHECK IN FORM"
// END OF FORMS

let toothNumberRequired: [String] = [""]
//Replace '""' with form names
let consentIndex: Int = 5


